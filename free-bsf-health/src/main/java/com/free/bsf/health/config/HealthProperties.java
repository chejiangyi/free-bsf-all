package com.free.bsf.health.config;

import com.free.bsf.core.util.ContextUtils;
import com.free.bsf.core.util.PropertyUtils;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author: chejiangyi
 * @version: 2019-07-24 21:03
 **/
public class HealthProperties {
    public static String Project="Health";
    public static String SpringApplictionName="spring.application.name";
    public static String BsfHealthEnabled="bsf.health.enabled";
    public static String BsfEnv = "bsf.env";
    public static String BsfHealthMemoryParseEnabled="bsf.health.memoryParse.enabled";
    public static String BsfHealthMemoryParseReferenceEnabled="bsf.health.memoryParse.referenceStat.enabled";
    public static String BsfHealthMemoryParseRequestEnabled="bsf.health.memoryParse.RequestStat.enabled";
    public static String ManagementEndpointsEnabledByDefault="management.endpoints.enabled-by-default";
    public static final String BsfHealthWebEnabled="bsf.health.web.enabled";

    /**
     * 是否开启健康检查
     * @return
     */
    public static boolean getHealthEnabled(){
        return PropertyUtils.getPropertyCache("bsf.health.enabled",false);
    }

    /**
     * 健康检查时间间隔 秒
     * @return
     */
    public static int getHealthTimeSpan(){return PropertyUtils.getPropertyCache("bsf.health.timespan",10);}

}
