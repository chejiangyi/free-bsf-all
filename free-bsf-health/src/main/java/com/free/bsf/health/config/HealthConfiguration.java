package com.free.bsf.health.config;

import com.free.bsf.core.config.BsfConfiguration;
import com.free.bsf.core.util.LogUtils;
import com.free.bsf.core.util.WebUtils;
import com.free.bsf.health.dump.DumpProvider;
import com.free.bsf.health.export.ExportProvider;
import com.free.bsf.health.filter.*;
import com.free.bsf.health.base.HealthCheckProvider;
import com.free.bsf.health.warn.WarnProvider;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.Ordered;

/**
 * @author: chejiangyi
 * @version: 2019-07-24 13:45
 **/
@Configuration
@ConditionalOnProperty(name = "bsf.health.enabled", havingValue = "true")
@Import(value = {BsfConfiguration.class,HealthSqlMybatisConfiguration.class,HealthMemoryParseConfiguration.class})
public class HealthConfiguration {
	@ConditionalOnProperty(name = "bsf.health.warn.enabled", havingValue = "true")
	@Bean(destroyMethod = "close")
	public WarnProvider getWarnProvider() {
		LogUtils.info(HealthConfiguration.class, HealthProperties.Project, "报警服务注册成功");
		return new WarnProvider();
	}

	@ConditionalOnProperty(name = "bsf.health.check.enabled", havingValue = "true")
	@Bean(destroyMethod = "close")
	public HealthCheckProvider getHealthCheckProvider() {
		LogUtils.info(HealthConfiguration.class, HealthProperties.Project, "自动健康检查服务注册成功");
		return new HealthCheckProvider();
	}

	@Bean
	@ConditionalOnWebApplication
	@ConditionalOnProperty(name = "bsf.health.report.enabled", havingValue = "true", matchIfMissing = true)
	public FilterRegistrationBean healthReportFilter() {
		FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
		// 优先注入
		filterRegistrationBean.setOrder(Ordered.LOWEST_PRECEDENCE);
		filterRegistrationBean.setFilter(new HealthReportFilter());
		filterRegistrationBean.setName("healthReportFilter");
		filterRegistrationBean.addUrlPatterns("/bsf/health/*");
		LogUtils.info(HealthConfiguration.class, HealthProperties.Project,
				"health报表注册成功,访问:" + WebUtils.getBaseUrl() + "/bsf/health/");
		return filterRegistrationBean;
	}


	@ConditionalOnProperty(name = "bsf.health.export.enabled", havingValue = "true")
	@Bean(initMethod = "start", destroyMethod = "close")
	public ExportProvider getExportProvider() {
		LogUtils.info(HealthConfiguration.class, HealthProperties.Project, "自动上传健康报表服务注册成功");
		return new ExportProvider();
	}

	@ConditionalOnProperty(name = "bsf.health.dump.enabled", havingValue = "true")
	@Bean
	public DumpProvider dumpProvider() {
		return new DumpProvider();
	}

	@ConditionalOnProperty(name = "bsf.health.dump.enabled", havingValue = "true")
	@Bean
	@ConditionalOnWebApplication
	public FilterRegistrationBean dumpFilter() {
		FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
		// 优先注入
		filterRegistrationBean.setOrder(Ordered.LOWEST_PRECEDENCE);
		filterRegistrationBean.setFilter(new DumpFilter());
		filterRegistrationBean.setName("bsfDumpFilter");
		filterRegistrationBean.addUrlPatterns("/bsf/health/dump/*");
		LogUtils.info(HealthConfiguration.class, HealthProperties.Project,
				"health dump注册成功,访问:" + WebUtils.getBaseUrl() + "/bsf/health/dump/");
		return filterRegistrationBean;
	}

	@ConditionalOnProperty(name = "bsf.health.ping.enabled", havingValue = "true", matchIfMissing = true)
	@Bean
	@ConditionalOnWebApplication
	public FilterRegistrationBean pingFilter() {
		FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
		// 优先注入
		filterRegistrationBean.setOrder(Ordered.LOWEST_PRECEDENCE);
		filterRegistrationBean.setFilter(new PingFilter());
		filterRegistrationBean.setName("bsfPingFilter");
		filterRegistrationBean.addUrlPatterns("/bsf/health/ping/*");
		LogUtils.info(HealthConfiguration.class, HealthProperties.Project,
				"health ping注册成功,访问:" + WebUtils.getBaseUrl() + "/bsf/health/ping/");
		return filterRegistrationBean;
	}

	@ConditionalOnProperty(name = "bsf.health.status.enabled", havingValue = "true", matchIfMissing = true)
	@Bean
	@ConditionalOnWebApplication
	public FilterRegistrationBean statusFilter() {
		FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
		// 优先注入
		filterRegistrationBean.setOrder(Ordered.LOWEST_PRECEDENCE);
		filterRegistrationBean.setFilter(new StatusFilter());
		filterRegistrationBean.setName("bsfStatusFilter");
		filterRegistrationBean.addUrlPatterns("/bsf/health/status/*");
		LogUtils.info(HealthConfiguration.class, HealthProperties.Project,
				"health status注册成功,访问:" + WebUtils.getBaseUrl() + "/bsf/health/status/");
		return filterRegistrationBean;
	}

	@ConditionalOnProperty(name = HealthProperties.BsfHealthWebEnabled, havingValue = "true", matchIfMissing = true)
	@Bean
	@ConditionalOnClass(name = "org.aspectj.lang.annotation.Aspect")
	public WebControllerAspect webControllerAspect(){
		return new WebControllerAspect();
	}
}
