package com.free.bsf.health.config;

import com.free.bsf.core.util.ContextUtils;
import com.free.bsf.core.util.PropertyUtils;
import com.free.bsf.core.util.StringUtils;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author: chejiangyi
 * @version: 2019-08-14 12:07
 **/
public class ExportProperties {
    /**
     * 上传报表循环间隔时间 秒
     * @return
     */
    public static int getExportTimeSpan(){
        return PropertyUtils.getPropertyCache("bsf.health.export.timespan",30);
    }

    public static String[] getElkDestinations(){
        String address=PropertyUtils.getPropertyCache("bsf.health.export.elk.destinations","");
        if(!StringUtils.isEmpty(address)){
            return address.split(",");
        }
        address=PropertyUtils.getPropertyCache("bsf.elk.destinations","");
        if(!StringUtils.isEmpty(address)){
            return address.split(",");
        }
        return new String[]{};
    }

    public static boolean getElkEnabled(){
        return PropertyUtils.getPropertyCache("bsf.health.export.elk.enabled",false);
    }

    public static boolean getCatEnabled(){
        return PropertyUtils.getPropertyCache("bsf.health.export.cat.enabled",false);
    }

    public static String getCatServerUrl(){
        String url=PropertyUtils.getPropertyCache("bsf.health.export.cat.server.url","");
        if(!StringUtils.isEmpty(url)){
            return url;
        }
        url=PropertyUtils.getPropertyCache("cat.server.url","");
        if(!StringUtils.isEmpty(url)){
            return url;
        }
        return "";
    }
}
