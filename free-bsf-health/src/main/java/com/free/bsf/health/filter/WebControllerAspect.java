package com.free.bsf.health.filter;

import ch.qos.logback.core.util.ContextUtil;
import com.free.bsf.core.util.*;
import com.free.bsf.health.base.AbstractCollectTask;
import com.free.bsf.health.base.EnumWarnType;
import com.free.bsf.health.config.WarnProperties;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * @author: chejiangyi
 * @version: 2019-08-21 14:49
 * 切面获取入参和出参
 */
@Order(Integer.MAX_VALUE-1)
@Aspect
@Slf4j
public class WebControllerAspect {

    @Pointcut("@within(org.springframework.stereotype.Controller) " +
            "|| @within(org.springframework.web.bind.annotation.RestController)")
    public void pointcut() {

    }

    @Around("pointcut()")
    public Object handle(ProceedingJoinPoint joinPoint) throws Throwable {
        Object result=null;
        try {
            long s = System.currentTimeMillis();
            result = joinPoint.proceed();
            long t = System.currentTimeMillis()-s;
            checkUrlExecuteTime(joinPoint,t);
        }catch (Exception e){
            warnUrlError(joinPoint,e);
            throw e;
        }

        return result;
    }

    private String inputParams(ProceedingJoinPoint joinPoint) {
        try{
            return JsonUtils.serialize(joinPoint.getArgs());
        }catch (Exception e){
            return "";
        }
    }
    //sql 执行超时检查
    private void checkUrlExecuteTime(ProceedingJoinPoint joinPoint, long timeSpan){
        HttpServletRequest request = WebUtils.getRequest();
        if(timeSpan > WarnProperties.getWarnUrlExecuteTimeOut()&&request!=null){
            String uri = request.getRequestURI();
            if(StringUtils.hitCondition(WarnProperties.getWarnUrlExecuteExclude().toLowerCase(),uri.toLowerCase()))
                return;
            String inPutParam = inputParams(joinPoint);
            AbstractCollectTask.notifyMessage(EnumWarnType.WARN, "url执行超时", String.format("[耗时]%sms,[url]%s,[入参]%s",timeSpan,uri,inPutParam));
        }
    }
    //sql 执行出错
    private void warnUrlError(ProceedingJoinPoint joinPoint,Exception e){
        if(ReflectionUtils.hitClassCondition(e==null?null:e.getClass(),WarnProperties.getWarnUrlExecuteErrorExcludeClasses())){
           return;
        }
        HttpServletRequest request = WebUtils.getRequest();
        if(WarnProperties.getWarnUrlExecuteErrorEnabled()&&request!=null){
            String uri = request.getRequestURI();
            if(StringUtils.hitCondition(WarnProperties.getWarnUrlExecuteExclude().toLowerCase(),uri.toLowerCase()))
                return;
            String inPutParam = inputParams(joinPoint);
            AbstractCollectTask.notifyMessage(EnumWarnType.WARN, "url执行错误", String.format("[url]%s,[入参]%s,[错误]%s",uri,inPutParam, ExceptionUtils.getDetailMessage(e)));
        }
    }
}