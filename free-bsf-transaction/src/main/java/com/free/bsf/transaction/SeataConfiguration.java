package com.free.bsf.transaction;

import com.free.bsf.core.config.BsfConfiguration;
import com.free.bsf.core.config.CoreProperties;
import com.free.bsf.core.util.StringUtils;
import io.seata.rm.datasource.DataSourceProxy;
import io.seata.rm.datasource.xa.DataSourceProxyXA;
import io.seata.spring.annotation.GlobalTransactionScanner;
import io.seata.spring.annotation.datasource.SeataDataSourceBeanPostProcessor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.sql.DataSource;

@Configuration
@ConditionalOnProperty(name = "bsf.transaction.seata.enable", havingValue = "true")
public class SeataConfiguration {
//    @Primary
//    @Bean("dataSource")
//    public DataSource dataSource(DataSource druidDataSource) {
//        if(TransactionProperties.getDefault().getSeataDataSourceProxyMode().equalsIgnoreCase("at")) {
//            //AT 代理 二选一
//            return new DataSourceProxy(druidDataSource);
//        }else {
//            //XA 代理
//            return new DataSourceProxyXA(druidDataSource);
//        }
//    }


    @Bean({"seataDataSourceBeanPostProcessor"})
    @ConditionalOnMissingBean({SeataDataSourceBeanPostProcessor.class})
    public SeataDataSourceBeanPostProcessor seataDataSourceBeanPostProcessor() {
        return new SeataDataSourceBeanPostProcessor(TransactionProperties.getExcludes(), TransactionProperties.getSeataDataSourceProxyMode());
    }

    @Bean
    public GlobalTransactionScanner globalTransactionScanner() {
        String applicationName = CoreProperties.getApplicationName();
        String txServiceGroup = TransactionProperties.getSeataTxServiceGroup();
        return new GlobalTransactionScanner(applicationName, txServiceGroup);
    }

    @Bean
    @ConditionalOnWebApplication
    public WebMvcConfigurer webMvcConfigurer() {
        return new  WebMvcConfigurer() {
            @Override
            public void addInterceptors(InterceptorRegistry registry) {
                registry.addInterceptor(new SeataHandlerInterceptor()).addPathPatterns("/**");
            }
        };
    }

    @Bean
    @ConditionalOnClass(name="feign.RequestInterceptor")
    public FeignSeataRequestHeaderInterceptor feignSeataRequestHeaderInterceptor(){
        return new FeignSeataRequestHeaderInterceptor();
    }
}
