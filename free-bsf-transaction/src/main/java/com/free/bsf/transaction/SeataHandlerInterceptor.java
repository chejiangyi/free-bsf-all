package com.free.bsf.transaction;

import com.free.bsf.core.util.LogUtils;
import io.seata.core.context.RootContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SeataHandlerInterceptor implements HandlerInterceptor {
    public SeataHandlerInterceptor() {
    }

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        String xid = RootContext.getXID();
        String rpcXid = request.getHeader("Seataid");

        if (xid == null && rpcXid != null) {
            RootContext.bind(rpcXid);
        }

        return true;
    }

    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception e) {
        String rpcXid = request.getHeader("Seataid");
        if (!StringUtils.isEmpty(rpcXid)) {
            String unbindXid = RootContext.unbind();
            if (!rpcXid.equalsIgnoreCase(unbindXid)) {
                if (unbindXid != null) {
                    RootContext.bind(unbindXid);
                }
            }

        }
    }
}

