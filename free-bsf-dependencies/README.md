# dependencies 
标准项目依赖组件版本定义,所有的项目依赖都会集中到这里打包,并上传到nexus中被第三方使用。

## 依赖引用
```
<parent>
	<groupId>com.free.bsf</groupId>
	<artifactId>free-bsf-dependencies</artifactId>
	<version>1.7.1-SNAPSHOT</version>
</parent>

```
[依赖组件版本定义pom.xml](pom.xml)
