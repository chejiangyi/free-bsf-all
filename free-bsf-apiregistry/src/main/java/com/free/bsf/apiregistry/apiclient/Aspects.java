package com.free.bsf.apiregistry.apiclient;

import com.free.bsf.apiregistry.ApiRegistryProperties;
import com.free.bsf.apiregistry.base.*;
import com.free.bsf.apiregistry.loadbalance.BaseLoadBalance;
import com.free.bsf.apiregistry.rpcclient.RpcClientFactory;
import com.free.bsf.core.apiregistry.CoreRequestInterceptor;
import com.free.bsf.core.base.Callable;
import com.free.bsf.core.util.ContextUtils;
import com.free.bsf.core.util.StringUtils;
import com.free.bsf.core.util.WarnUtils;
import lombok.val;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;

import java.lang.reflect.Method;

public class Aspects {
    @Order(2)
    @Aspect
    public static class ApiClientAspect {
        private ClientAspect clientAspect = new ClientAspect();
        @Pointcut("@within(com.free.bsf.api.apiregistry.ApiClient)")
        public void pointCut(){};

        @Around("pointCut()")
        public Object handle(ProceedingJoinPoint joinPoint) throws Throwable {
            return clientAspect.aop(joinPoint);
        }
    }

    @Order(2)
    @Aspect
    public static class AllClientAspect  {
        private ClientAspect clientAspect = new ClientAspect();
        @Pointcut("@within(com.free.bsf.api.apiregistry.ApiClient) || @within(org.springframework.cloud.openfeign.FeignClient)")
        public void pointCut(){};

        @Around("pointCut()")
        public Object handle(ProceedingJoinPoint joinPoint) throws Throwable {
            return clientAspect.aop(joinPoint);
        }
    }

    public static class ClientAspect {
        public Object aop(ProceedingJoinPoint joinPoint) throws Throwable {
            if(!ApiRegistryProperties.getApiClientAspectEnabled()||
                    !ApiRegistryProperties.getEnabled()){
                return joinPoint.proceed();
            }
            val method = ((MethodSignature)joinPoint.getSignature()).getMethod();
            val apiClientInfo = ApiUtils.getApiClient(method);
            if(apiClientInfo ==null){
                throw new ApiRegistryException("未找到ApiClient或FeignClient注解信息");
            }
            val apiIgnore=ApiUtils.getApiIgnore(method);
            if(apiIgnore!=null){
                return joinPoint.proceed();
            }
            //测试模式
            if(ApiRegistryProperties.getTestEnabled()){
                val eurekaResult = joinPoint.proceed();
                String methodPath=method.getDeclaringClass().getName()+"."+method.getName();
                if(!StringUtils.hitCondition(ApiRegistryProperties.getTestSkipMethods().toLowerCase(),methodPath.toLowerCase())){
                    try{
                        val rpcResult = rpc(apiClientInfo,method,joinPoint);
                        ApiUtils.checkObjectEqual(eurekaResult,rpcResult);
                    }catch (Exception e){
                        String message="appName:"+apiClientInfo.getName()+" method:"+methodPath+" exp:"+e.getMessage();
                        WarnUtils.notify(WarnUtils.ALARM_ERROR,"apiRegistry 测试对比rpc结果异常",message);
                    }
                }
                return eurekaResult;
            }

            try {
                return rpc(apiClientInfo,method,joinPoint);
            }
            catch (Exception e){
                if(ApiRegistryProperties.getWarnEnabled()) {
                    WarnUtils.notify(WarnUtils.ALARM_ERROR, "apiRegistry rpc调用异常", e.getMessage());
                }
                throw e;
            }
        }

        protected Object rpc(ApiClientInfo apiClientInfo, Method method, ProceedingJoinPoint joinPoint){
            return this.retryExecute(()->{
                val loadBalance = ContextUtils.getBean( BaseLoadBalance.class,false);
                val hostPort = loadBalance.getAvailableHostPort(apiClientInfo.getName());
                String url = ApiUtils.getUrl(hostPort,apiClientInfo.getPath());
                if(StringUtils.isEmpty(url))
                    throw new ApiRegistryException("未找到服务host信息");

                //解析
                val requestParserInfo = new BaseApiClientParser.ApiClientParserInfo(apiClientInfo.getName(),url,method,joinPoint);
                val requestInfo = new ApiClientParser().parse(requestParserInfo);
                //拦截器
                val interceptors = ContextUtils.getBeans(CoreRequestInterceptor.class);
                if(interceptors!=null){
                    for(val i:interceptors){
                        i.append(requestInfo);
                    }
                }

                //请求
                val httpClient = RpcClientFactory.create();
                return httpClient.execute(requestInfo,method.getGenericReturnType());
            });
        }

        protected Object retryExecute(Callable.Func0 func0){
            ApiRegistryException exp=null;
            for(Integer i=0;i<ApiRegistryProperties.getRegistryFailRetryTimes();i++){
                try {
                    return func0.invoke();
                }catch (ApiRegistryHttpStateException exception){
                    val loadBalance = ContextUtils.getBean( BaseLoadBalance.class,false);
                    if(loadBalance!=null) {
                        loadBalance.addFail(exception.getAppName(),exception.getUrl());
                    }
                    exp=exception;
                }
            }
            throw exp;
        }
    }
}
