package com.free.bsf.apiregistry.base;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.free.bsf.api.apiregistry.ApiClient;
import com.free.bsf.api.apiregistry.ApiIgnore;
import com.free.bsf.apiregistry.ApiRegistryProperties;
import com.free.bsf.core.util.JsonUtils;
import com.free.bsf.core.util.ReflectionUtils;
import com.free.bsf.core.util.StringUtils;
import lombok.val;
import org.springframework.core.annotation.AnnotationUtils;

import java.lang.reflect.Method;

public class ApiUtils {
    public static Class FeignClientAnnotationClass = ReflectionUtils.tryClassForName(ApiRegistryProperties.FeignClientClassPath);
    public static ApiClientInfo getApiClient(Method method){
        val apiClient= AnnotationUtils.getAnnotation(method.getDeclaringClass(), ApiClient.class);
        if(apiClient != null){
            return new ApiClientInfo(StringUtils.nullToEmpty(apiClient.name()),StringUtils.nullToEmpty(apiClient.path()));
        }

        val feignClient= AnnotationUtils.getAnnotation(method.getDeclaringClass(),FeignClientAnnotationClass);
        if(feignClient != null){
            val name = ReflectionUtils.tryCallMethod(feignClient,"name",null,null);
            val path = ReflectionUtils.tryCallMethod(feignClient,"path",null,null);
            return new ApiClientInfo(StringUtils.nullToEmpty(name),StringUtils.nullToEmpty(path));
        }
        return null;
    }

    public static ApiIgnore getApiIgnore(Method method){
        val apiIgnore= method.getAnnotation(ApiIgnore.class);
        if(apiIgnore != null){
            return apiIgnore;
        }
        return null;
    }

    public static String getUrl(String host,String contextPath){
        if(StringUtils.isEmpty(host)){
            return "";
        }
        String url=host;
        if(!host.startsWith("http://")&&!host.startsWith("https://")){
            url="http://"+host;
        }
        if(!StringUtils.isEmpty(contextPath)){
            url=StringUtils.trimRight(url,'/')+contextPath;
        }
        return url;
    }

    public static void checkObjectEqual(Object from,Object to){
        if(from==null&&to==null)
            return;
        String message;
        try {
            val fromStr = JsonUtils.serialize(from);
            val toStr = JsonUtils.serialize(to);
            val fromJson = clearJsonNode(JsonUtils.Default.getMapper().readTree(fromStr));
            val toJson = clearJsonNode(JsonUtils.Default.getMapper().readTree(toStr));
            if(fromJson.equals(toJson))
                return;
            message ="checkObjectEqual对象不一致,fromJson:"+StringUtils.nullToEmpty(fromJson)+" toJson:"+StringUtils.nullToEmpty(toJson);
        }catch (Exception e){
            message ="checkObjectEqual时发生错误,error:"+e.getMessage();
        }
        throw new ApiRegistryException(message);
    }

    //清理多余节点
    private static JsonNode clearJsonNode(JsonNode jsonNode){
        if(jsonNode!=null){
            if(jsonNode.isObject()){
                ((ObjectNode)jsonNode).remove("debug");
            }
        }
        return jsonNode;
    }

}
