package com.free.bsf.apiregistry.rpcclient;

import com.fasterxml.jackson.core.type.TypeReference;
import com.free.bsf.apiregistry.ApiRegistryProperties;
import com.free.bsf.apiregistry.base.ApiRegistryException;
import com.free.bsf.apiregistry.base.ApiRegistryHttpStateException;
import com.free.bsf.apiregistry.code.CodeFactory;
import com.free.bsf.core.apiregistry.RequestInfo;
import com.free.bsf.core.util.HttpUtils;
import com.free.bsf.core.util.JsonUtils;
import com.free.bsf.core.util.StringUtils;
import lombok.val;

import java.lang.reflect.Type;

/**
 * HttpUrlConnection实现
 */
public class HttpUrlConnectionRpcClient implements IRpcClient {
    public  <T>T execute(RequestInfo requestInfo, Type cls){
        val response = HttpUtils.request(new HttpUtils.HttpRequest(
                requestInfo.getUrl(),
                requestInfo.getMethod(),
                requestInfo.getHeader(),
                requestInfo.getBody(),
                ApiRegistryProperties.getHttpUrlConnectionConnectTimeOut(),
                ApiRegistryProperties.getHttpUrlConnectionReadTimeOut(),
                ApiRegistryProperties.getHttpUrlConnectionPoolEnabled()));
        if(!response.isSuccess()){
            throw new ApiRegistryHttpStateException(requestInfo.getAppName(),StringUtils.nullToEmpty(requestInfo.getUrl()),response.getCode());
        }
        val code = CodeFactory.create(response.getHeader().get("Content-Type"));
        return code.decode(response.getBody(),cls);
    }
}
