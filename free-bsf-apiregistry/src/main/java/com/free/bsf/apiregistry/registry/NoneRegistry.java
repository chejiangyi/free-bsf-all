package com.free.bsf.apiregistry.registry;

import java.util.*;

/**
 * 空注册中心
 */
public class NoneRegistry extends BaseRegistry {
    private Map<String, List<String>> cacheServerList=new HashMap<>();
    public NoneRegistry() {
    }

    @Override
    public Map<String, List<String>> getServerList() {
        return cacheServerList;
    }
}
