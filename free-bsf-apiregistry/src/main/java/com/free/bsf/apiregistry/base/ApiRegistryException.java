package com.free.bsf.apiregistry.base;

import com.free.bsf.core.base.BsfException;

public class ApiRegistryException extends BsfException {
    public ApiRegistryException(String message,Exception exp){
        super(message, exp);
    }
    public ApiRegistryException(String message){
        super(message);
    }
    public ApiRegistryException(Exception exp){
        super(exp);
    }
}
