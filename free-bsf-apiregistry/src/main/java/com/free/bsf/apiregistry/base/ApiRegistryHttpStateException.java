package com.free.bsf.apiregistry.base;

import com.free.bsf.core.base.BsfException;
import com.free.bsf.core.util.StringUtils;
import lombok.Getter;

public class ApiRegistryHttpStateException extends ApiRegistryException {
    @Getter
    private String appName;
    @Getter
    private String url;
    @Getter
    private int stateCode;
    public ApiRegistryHttpStateException(String appName,String url,int stateCode){
        super("url:"+ StringUtils.nullToEmpty(url)+",返回状态码:"+stateCode);
        this.appName = appName;
        this.url=url;
        this.stateCode=stateCode;
    }
}
