package com.free.bsf.api.apiregistry;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface ApiClient {
    String name() default "";
    String path() default "";
}
