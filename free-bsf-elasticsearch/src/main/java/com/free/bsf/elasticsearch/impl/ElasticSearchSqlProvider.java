package com.free.bsf.elasticsearch.impl;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.free.bsf.core.base.Callable;
import com.free.bsf.core.util.*;
import com.free.bsf.elasticsearch.base.Param;
import lombok.val;
import com.free.bsf.core.serialize.JsonSerializer;
import com.free.bsf.elasticsearch.ElasticSearchProperties;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.elasticsearch.client.RestHighLevelClient;


public class ElasticSearchSqlProvider extends ElasticSearchProvider{
	public ElasticSearchSqlProvider(RestHighLevelClient elasticsearchClient){
		super(elasticsearchClient);
	}

	public <T> List<T> searchListBySql(String sql,Param param, Class<T> clazz){
		return null;
	}

	public <T> List<T> searchListBySql(String sql, Class<T> clazz) {
		return null;
	}

	public <T> T searchObjectBySql(String sql,Param param, Class<T> clazz){
		return null;
	}

	public <T> T searchObjectBySql(String sql, Class<T> clazz) {
		return null;
	}

	public void deleteBySql(String sql,Param param){
	}

	public void deleteBySql(String sql) {
	}

	@Override
	public void close() {
		super.close();
	}

	protected static String DATA_FORMAT="yyyy-MM-dd'T'HH:mm:ss";
	protected static ObjectMapper objectMapper = JsonSerializer.createObjectMapper()
			.setDateFormat(new SimpleDateFormat(DATA_FORMAT));
	protected <T> ParameterizedType getListType(Class<T> clazz){
		val type = new ParameterizedType() {
			public String getTypeName() {
				return List.class.getTypeName();
			}
			public Type[] getActualTypeArguments() {
				return new Type[]{clazz};
			}
			public Type getRawType() {
				return List.class;
			}
			public Type getOwnerType() {
				return null;
			}
		};
		return type;
	}
	public String formatDate(Date date){
		return DateFormatUtils.format(date,DATA_FORMAT);
	}
	protected String toJson(Object obj){
		JsonSerializer jsonSerializer = new JsonSerializer();
		jsonSerializer.setObjectMapper(objectMapper);
		String json = jsonSerializer.serialize(obj);
		return json;
	}
	protected <T>T fromJson(String json,Type type){
		JsonSerializer jsonSerializer = new JsonSerializer();
		jsonSerializer.setObjectMapper(objectMapper);
		return jsonSerializer.deserialize(json,type);
	}
	protected <T> T pintSql(String sql,Callable.Func0<T> func0){
		try{
			return func0.invoke();
		}catch (Throwable e){
			if(PropertyUtils.getPropertyCache(ElasticSearchProperties.BsfElasticSearchPrintSql,true)){
				LogUtils.info(ElasticSearchSqlProvider.class,ElasticSearchProperties.Project,sql);
			}
			throw e;
		}
	}

}
