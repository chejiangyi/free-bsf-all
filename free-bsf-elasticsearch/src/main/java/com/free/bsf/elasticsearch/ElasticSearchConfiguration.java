package com.free.bsf.elasticsearch;

import com.free.bsf.core.config.BsfConfiguration;
import com.free.bsf.core.util.LogUtils;
import com.free.bsf.core.util.StringUtils;
import com.free.bsf.elasticsearch.base.ElasticSearchException;
import com.free.bsf.elasticsearch.impl.ElasticSearchDruidSqlProvider;
import com.free.bsf.elasticsearch.impl.ElasticSearchRestSqlProvider;
import com.free.bsf.elasticsearch.impl.ElasticSearchSqlProvider;
import org.apache.http.HttpHost;
import org.elasticsearch.client.ElasticsearchClient;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.data.elasticsearch.ElasticsearchDataAutoConfiguration;
import org.springframework.context.annotation.*;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;

@Configuration
@Import(value = {BsfConfiguration.class})
@ConditionalOnProperty(name = ElasticSearchProperties.BsfElasticSearchEnabled, havingValue = "true")
public class ElasticSearchConfiguration implements InitializingBean {
	@Override
	public void afterPropertiesSet() {
		LogUtils.info(ElasticSearchConfiguration.class,ElasticSearchProperties.Project,"连接初始化成功!!! "+ElasticSearchProperties.BsfElasticSearchTcpPort+"=" + ElasticSearchProperties.getServer());
	}

	@Bean(name = "elasticsearchClient",destroyMethod = "close")
	@Lazy
	public RestHighLevelClient getElasticsearchClient(){
		var server=ElasticSearchProperties.getServer();
		if (StringUtils.isEmpty(server)) {
			throw new ElasticSearchException("bsf.elasticsearch.server 未配置");
		}

		ClientConfiguration clientConfiguration = ClientConfiguration.builder()
				.connectedTo(ElasticSearchProperties.getServer()+":"+ElasticSearchProperties.getRestPort())
				.build();

		return RestClients.create(clientConfiguration).rest();
	}

	@Bean(destroyMethod = "close")
	@Lazy
	@DependsOn("elasticsearchClient")
	public ElasticSearchDruidSqlProvider elasticSearchDruidSqlProvider(RestHighLevelClient elasticsearchClient) {
		return new ElasticSearchDruidSqlProvider(elasticsearchClient);
	}

	@Bean(destroyMethod = "close")
	@Lazy
	@DependsOn("elasticsearchClient")
	@Primary
	public ElasticSearchRestSqlProvider elasticSearchRestSqlProvider(RestHighLevelClient elasticsearchClient) {
		return new ElasticSearchRestSqlProvider(elasticsearchClient);
	}

}
