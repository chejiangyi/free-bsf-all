package com.free.bsf.elasticsearch.initializer;

import com.free.bsf.core.config.CoreProperties;
import com.free.bsf.core.util.LogUtils;
import com.free.bsf.core.util.PropertyUtils;
import com.free.bsf.core.util.ReflectionUtils;
import com.free.bsf.core.util.StringUtils;
import com.free.bsf.elasticsearch.ElasticSearchProperties;
import lombok.val;
import org.apache.logging.log4j.util.Strings;
import org.springframework.boot.autoconfigure.AutoConfigurationImportFilter;
import org.springframework.boot.autoconfigure.AutoConfigurationMetadata;
import org.springframework.boot.autoconfigure.data.elasticsearch.ElasticsearchDataAutoConfiguration;
import org.springframework.boot.autoconfigure.data.elasticsearch.ElasticsearchRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.data.elasticsearch.ReactiveElasticsearchRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.data.elasticsearch.ReactiveElasticsearchRestClientAutoConfiguration;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

import java.util.Arrays;
import java.util.List;

public class ElasticsearchApplicationContextInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext>, AutoConfigurationImportFilter {
    @Override
    public void initialize(ConfigurableApplicationContext context) {
        ConfigurableEnvironment environment = context.getEnvironment();
        if("false".equalsIgnoreCase(environment.getProperty(CoreProperties.BsfEnabled))){
            return;
        }
        String propertyValue = environment.getProperty(ElasticSearchProperties.SpringApplicationName);
        if (!Strings.isEmpty(propertyValue)) {
            //
            PropertyUtils.setDefaultInitProperty(ElasticsearchApplicationContextInitializer.class,
                    ElasticSearchProperties.Project,ElasticSearchProperties.ManagementHealthElasticSearchEnabled,"false");
        }
    }

    @Override
    public boolean[] match(String[] autoConfigurationClasses, AutoConfigurationMetadata autoConfigurationMetadata) {
        val excludes= List.of(ElasticsearchDataAutoConfiguration.class.getName(),
                ElasticsearchRepositoriesAutoConfiguration.class.getName(),
                ReactiveElasticsearchRepositoriesAutoConfiguration.class.getName()
        );
        String msg="";
        boolean[] matches = new boolean[autoConfigurationClasses.length];
        for (int i = 0; i < matches.length; i++) {
            val cls = autoConfigurationClasses[i];
            // 当读取到AutoConfiguration自动配置类时, 如果配置文件中*configuration.enabled为true,则加载该自动配置类
            if(PropertyUtils.getPropertyCache(ElasticSearchProperties.BsfElasticSearchAutoConfigurationImportFilterEnabled,true)
            &&cls!=null&&excludes.contains(cls)){
                matches[i] =PropertyUtils.getPropertyCache(ElasticSearchProperties.BsfElasticSearchEnabled,false);
                if(!matches[i]){
                    msg+=ReflectionUtils.tryClassForName(cls).getSimpleName()+",";
                }
            }else{
                matches[i]=true;
            }
        }
        if(!StringUtils.isEmpty(msg)){
            LogUtils.info(ElasticsearchApplicationContextInitializer.class,ElasticSearchProperties.Project,"跳过自动装配:"+ StringUtils.trim(msg,','));
        }
        return matches;
    }
}
