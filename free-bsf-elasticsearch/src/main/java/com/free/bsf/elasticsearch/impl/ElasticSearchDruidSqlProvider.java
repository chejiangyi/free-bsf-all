package com.free.bsf.elasticsearch.impl;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.pool.ElasticSearchDruidDataSourceFactory;
import com.free.bsf.core.db.DbConn;
import com.free.bsf.core.util.ConvertUtils;
import com.free.bsf.elasticsearch.ElasticSearchProperties;
import com.free.bsf.elasticsearch.base.ElasticSearchException;
import com.free.bsf.elasticsearch.base.Param;
import lombok.Getter;
import lombok.val;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.elasticsearch.client.RestHighLevelClient;

import java.util.List;
import java.util.Properties;


public class ElasticSearchDruidSqlProvider extends ElasticSearchSqlProvider{
	@Getter
	protected DruidDataSource druidDataSource;
	public ElasticSearchDruidSqlProvider(RestHighLevelClient elasticsearchClient){
		super(elasticsearchClient);
		Properties properties = new Properties();
		properties.put("url", "jdbc:elasticsearch://"+ElasticSearchProperties.getServer()+":"+ElasticSearchProperties.getTcpPort()+"/");
		try {
			this.druidDataSource = (DruidDataSource) ElasticSearchDruidDataSourceFactory.createDataSource(properties);
		}catch (Exception exception){
			throw new ElasticSearchException(exception);
		}
	}
	public <T> List<T> searchListBySql(String sql,Param param, Class<T> clazz){
		return searchListBySql(param.build(sql),clazz);
	}
	public <T> List<T> searchListBySql(String sql, Class<T> clazz) {
		return ElasticSearchMonitor.hook().run("searchBySql", () -> {
			return pintSql(sql,()->{
				try(DbConn conn=new DbConn(this.druidDataSource)) {
					val rs = conn.executeList(sql, new Object[]{});
					List<T> list = fromJson(toJson(rs), getListType(clazz));
					return list;
			}});
		});
	}
	public <T> T searchObjectBySql(String sql,Param param, Class<T> clazz){
		return searchObjectBySql(param.build(sql),clazz);
	}

	public <T> T searchObjectBySql(String sql, Class<T> clazz) {
		return ElasticSearchMonitor.hook().run("searchBySql", () -> {
			return pintSql(sql,()->{
				try(DbConn conn=new DbConn(this.druidDataSource)){
					return ConvertUtils.tryConvert(conn.executeScalar(sql, new Object[]{}),clazz);
			}});
		});
	}
	public void deleteBySql(String sql,Param param){
		deleteBySql(sql,param);
	}

	public void deleteBySql(String sql) {
		ElasticSearchMonitor.hook().run("deleteBySql", () -> {
			pintSql(sql,()->{
				try(DbConn conn=new DbConn(this.druidDataSource)){
					try {
						conn.getConn().prepareStatement(sql).executeQuery();
					}catch (Exception e){
						if((e.getCause()!=null&&e.getCause()instanceof NullPointerException))
						{
							//底层不返回值
						}else {
							throw new ElasticSearchException(new DbConn.DbException("es deleteBySql异常",sql,e));
						}
					}
					return true;
				}
			});
		});
	}

	@Override
	public void close() {
		if(this.druidDataSource!=null){
			this.druidDataSource.close();
		}
	}
}
