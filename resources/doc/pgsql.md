## bsf 使用pgsql注意事项
#### 常见springboot+mybatis plus+pgsql的坑须知
https://www.cnblogs.com/haolb123/p/14263133.html
#### pgsql 推荐兼容方案
1) 尽量采用同mysql的场景类型
#### 常用配置
* 生成代码 
[mybatis puls pgsql代码生成](https://gitee.com/chejiangyi/mybatis-plus-toolkit/blob/master/doc/config.properties)
* 引用pgsql驱动包
```
    <dependency>
        <groupId>org.postgresql</groupId>
        <artifactId>postgresql</artifactId>
    </dependency>
```
* 配置pgsql链接地址
```
# springboot默认数据源,pgsql配置
spring.datasource.driver-class-name=org.postgresql.Driver
spring.datasource.url=jdbc:postgresql://127.0.0.1:5432/demo
spring.datasource.username=root
spring.datasource.password=
```
```
# shardingjdbc数据源,pgsql配置
尚未验证,待完善
```