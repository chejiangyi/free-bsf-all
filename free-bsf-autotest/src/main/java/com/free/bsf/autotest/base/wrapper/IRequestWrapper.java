package com.free.bsf.autotest.base.wrapper;

import com.free.bsf.core.util.StringUtils;

public interface IRequestWrapper {
     byte[] getBody();
}
