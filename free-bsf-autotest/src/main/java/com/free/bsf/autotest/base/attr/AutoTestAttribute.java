package com.free.bsf.autotest.base.attr;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.core.annotation.Order;

import java.lang.annotation.*;
import java.util.HashMap;
import java.util.Map;

@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface AutoTestAttribute {
    Attributes.ApiTypeEnum apiType() default Attributes.ApiTypeEnum.NONE;
    Attributes.TestEnum test() default Attributes.TestEnum.NONE;
    Attributes.LevelEnum level() default Attributes.LevelEnum.COMMON;
    String author() default "";
    String extMap() default "{}";




}