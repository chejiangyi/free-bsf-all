package com.free.bsf.autotest.base.attr;

import com.free.bsf.autotest.AutoTestProperties;
import com.free.bsf.autotest.base.AutoTestUtil;
import com.free.bsf.core.config.CoreProperties;
import com.free.bsf.core.util.JsonUtils;
import com.free.bsf.core.util.PropertyUtils;
import com.free.bsf.core.util.StringUtils;
import com.free.bsf.core.util.WebUtils;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: chejiangyi
 * @version: 2019-08-21 14:49
 * 切面获取入参和出参
 */
@Aspect
@Slf4j
public class AutoTestAttributeAspect {
    @Around("@annotation(com.free.bsf.autotest.base.attr.AutoTestAttribute)")
    public Object handle(ProceedingJoinPoint joinPoint) throws Throwable {
        if(WebUtils.getRequest()!=null && AutoTestUtil.hasReWriteRequest(WebUtils.getRequest())){
            val method = (MethodSignature)joinPoint.getSignature();
            val attribute = method.getMethod().getAnnotation(AutoTestAttribute.class);
            WebUtils.getRequest().setAttribute(AutoTestProperties.AutoTestAttribute,attributeToJson(attribute));
        }
        return joinPoint.proceed();
    }

    private String attributeToJson(AutoTestAttribute autoTestAttribute){
        val env=PropertyUtils.getPropertyCache(CoreProperties.BsfEnv,"");
        val map = new HashMap<String,Object>() {
            {
                put("test", autoTestAttribute.test().getName());
                put("author", autoTestAttribute.author());
                put("apiType", autoTestAttribute.apiType().getName());
                put("level", autoTestAttribute.level().getName());
                if(!StringUtils.isEmpty(env)) {
                    put("env", PropertyUtils.getPropertyCache(CoreProperties.BsfEnv, ""));
                }
                putAll(JsonUtils.deserialize(autoTestAttribute.extMap(), Map.class));
            }
        };
        return JsonUtils.serialize(map);
    }
}