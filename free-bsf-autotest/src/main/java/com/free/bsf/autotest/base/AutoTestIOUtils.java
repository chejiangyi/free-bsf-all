package com.free.bsf.autotest.base;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class AutoTestIOUtils {
    public static byte[] toArrays(InputStream input) throws IOException {
        try(ByteArrayOutputStream output = new ByteArrayOutputStream()) {
            byte[] buffer = new byte[1024];
            int n = 0;
            while (-1 != (n = input.read(buffer))) {
                output.write(buffer, 0, n);
            }
            output.flush();
            return output.toByteArray();
        }
    }

    public static byte[] toArrays(String str){
        return str.getBytes(StandardCharsets.UTF_8);
    }

    public static String toString(byte[] bytes){
        return new String(bytes,StandardCharsets.UTF_8);
    }
}
