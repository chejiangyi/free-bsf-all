package com.free.bsf.autotest.store;

import com.free.bsf.autotest.AutoTestProperties;
import com.free.bsf.core.base.BsfEventEnum;
import com.free.bsf.core.common.PropertyCache;
import com.free.bsf.core.common.Pubsub;
import com.free.bsf.core.config.CoreProperties;
import com.free.bsf.core.util.LogUtils;
import com.free.bsf.core.util.PropertyUtils;
import com.free.bsf.core.util.ThreadUtils;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

import java.util.HashMap;

public class StoreManager implements AutoCloseable {
    public static StoreManager Default = new StoreManager();
    private BaseStore store = null;
    private Object lock = new Object();

    public StoreManager(){
        ThreadUtils.shutdown(()->{
            close();
        },1,false);

        PropertyCache.Default.listenUpdateCache("自动化测试配置变更",(data)->{
            boolean isclose=false;
            if(data!=null&&data.size()>0){
                for(val e:data.entrySet()){
                    if(e.getKey().startsWith(AutoTestProperties.Project.toLowerCase()+".store.")){
                        isclose=true;
                    }
                }
            }
            if(isclose){
                close();
            }
        });
    }

    public BaseStore createOrGet(){
        if(store==null){
            synchronized (lock){
                if(store==null) {
                    if ("mysql".equalsIgnoreCase(PropertyUtils.getPropertyCache(AutoTestProperties.AutoTestStoreType, "mysql"))) {
                        store = new MysqlStore();
                    }
                }
            }
        }
        return store;
    }

    public void close(){
        try{
            if(store!=null) {
                synchronized (lock) {
                    if(store!=null) {
                        store.close();
                        LogUtils.info(this.getClass(),AutoTestProperties.Project,"关闭成功");
                    }
                }
            }
        }
        catch (Exception e){
            LogUtils.error(this.getClass(),AutoTestProperties.Project,"关闭异常",e);
        }
    }
}
