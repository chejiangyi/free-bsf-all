package com.free.bsf.autotest.store;

import com.free.bsf.autotest.AutoTestContext;
import com.free.bsf.autotest.AutoTestProperties;
import com.free.bsf.autotest.base.RequestInfo;
import com.free.bsf.core.util.LogUtils;
import com.free.bsf.core.util.PropertyUtils;
import com.free.bsf.core.util.ThreadUtils;
import lombok.val;

public class BaseStore implements AutoCloseable {
    protected volatile boolean isClose=false;
    public BaseStore(){

    }
    public void run(){
        ThreadUtils.system().submit("自动化测试采样任务",()->{
            while (!ThreadUtils.system().isShutdown()&&!isClose) {
                try {
                    val requestInfos = AutoTestContext.Context.getRequestCache().poll();
                    if (requestInfos != null && requestInfos.length > 0) {
                        innerRun(requestInfos);
                    }
                } catch (Exception exp) {
                    LogUtils.error(this.getClass(),AutoTestProperties.Project, "自动化测试采样任务异常", exp);
                }
                ThreadUtils.sleep(PropertyUtils.getPropertyCache(AutoTestProperties.AutoTestStoreFlushTimeSpan, 5000));
            }
        });
    }

    protected void innerRun(RequestInfo[] requestInfos){

    }

    public void close(){
        isClose=true;
    }
}
