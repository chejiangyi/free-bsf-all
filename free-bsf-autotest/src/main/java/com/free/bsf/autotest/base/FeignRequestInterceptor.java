package com.free.bsf.autotest.base;

import com.free.bsf.autotest.AutoTestProperties;
import com.free.bsf.core.apiregistry.CoreRequestInterceptor;
import com.free.bsf.core.apiregistry.RequestInfo;
import com.free.bsf.core.util.WebUtils;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.val;

import java.util.HashMap;
import java.util.Map;

/**
 * Feign HTTP调用请求头增加RequestUtil.REQUEST_TRACEID参数
 * @author: chejiangyi
 * @version: 2019-08-30 13:21
 **/
public class FeignRequestInterceptor implements RequestInterceptor, CoreRequestInterceptor {
    @Override
    public void apply(RequestTemplate requestTemplate) {
       val header = newHeader();
       if(header!=null){
           for(val kv:header.entrySet()){
               requestTemplate.header(kv.getKey(),kv.getValue());
           }
       }
    }

    private Map<String,String> newHeader(){
        if (WebUtils.getRequest() != null) {
            Map<String,String> header = new HashMap<>();
            val traceId = AutoTestUtil.getTraceId(WebUtils.getRequest());
            if (traceId != null) {
                header.put(AutoTestProperties.AutoTestTraceId, traceId);
            }
            return header;
        }
        return null;
    }

    @Override
    public void append(RequestInfo request) {
        val header = newHeader();
        if(header!=null){
            for(val kv:header.entrySet()){
                request.getHeader().put(kv.getKey(),kv.getValue());
            }
        }
    }
}