## Redis组件集成

### 介绍
公司标准使用Cluster Redis模式,当前组件用于同一封装及优化客户端参数，提供常用工具服务类，简化业务端使用。  
关于Redis的集群搭建及使用，请详见[官方](https://redis.io/)

### 依赖引用
``` 
<dependency>
	<artifactId>free-bsf-redis</artifactId>
	<groupId>com.free.bsf</groupId>
	<version>1.7.1-SNAPSHOT</version>
</dependency>
```

### redis基本需知
1. 所有redis值设置,都必须要带有过期时间,不允许存永久的数据。最大的过期时间不应该超过1天。  
缓存过期设计上要考虑瞬间穿透问题,缓存过期时间在设计上要考虑离散。
2. 禁止使用key:xxx*这种模块匹配查询,会中断阻塞所有redis请求,性能极差。
### 配置说明
[进阶配置](doc/README-进阶配置.md)
```
#bsf redis是否开启支持
bsf.redis.enabled = false
```

### RedisCache 缓存代码示例
* 注解方式
```
    #key支持spel表达式,timeout 单位为秒
    @RedisCache(key = "'user-'+#userId", timeout = 60)
    public User callComplex(long userId) {
        return new User(userId, "hello: " + System.currentTimeMillis(), Arrays.asList(new User.Job(userId + "001", "haha")));
    }
```
* 函数方式
```
    public List<User> callback(long userId) {
        List<User> list = redisProvider.cache("user-" + userId, 60, () -> Arrays.asList(new User(userId, "hello: " + userId, Arrays.asList(new User.Job(userId + "001", "haha")))), new TypeReference<List<User>>() {
        }.getType());
        System.out.println(list);
        return list;
    }
```

### 分布式锁 代码示例
* redis实现分布式锁可自动续约,也可以自定义超时时间。
```
     //加锁,如果冲突,则一直等待
     redisProvider.lock(key, () -> {
        try {
            callable.call();
        } catch (Exception e) {
            e.printStackTrace();
        }
     });
     //尝试加锁,不行直接放弃
     boolean result = redisProvider.tryLock(key, () -> {
        try {
            callable.call();
        } catch (Exception e) {
            e.printStackTrace();
        }
     });
```

### RedisLeaderElection 领导者选举算法
通过分布式锁实现领导者选举算法,使用时建议阅读代码,理解原理。
```
    RedisLeaderElection leaderElection = new RedisLeaderElection();
    //异步通知进行选举
    leaderElection.doElection(task);
    //异步监听器
    leaderElection.getListeners().add((name,isMaster)->{
        if(isMaster){
            System.out.println(threadName+ "【主节点】:" + isMaster);
        }else{
            System.out.println(threadName+ "【从节点】:" + isMaster);
        }
    });
    //退出选举
    leaderElection.exitElection();
    //当前是否是主节点
    leaderElection.isMaster();
```

### 分布式限流 代码示例
* redis实现分布式限流,支持计数,滑动窗口,令牌桶三种算法。 
* sdk集成模式[推荐]
```
  val redis = ContextUtils.getBean(RedisProvider.class,false);
  if (redis.limit("测试限流", 1, 10,RedisLimitType.Count)) {
        //正常访问
  } else {
        //已限流
  }
```
* 原生模式[不推荐]
```
  val jRedis = new Jedis({IP},6379);
  val redis = RedisLimitFactory.create(jRedis,RedisLimitType.Slide);
  if (redis.visit("测试限流", 1, 10)) {
        //正常访问
  } else {
        //已限流
  }
```
注意:计数和滑动窗口支持集群模式,令牌桶不支持集群模式。

### 标准redis访问接口示范[推荐]
[标准Api接口文档](doc/README-Api.md)  
使用示范如下:
```
	@Autowired
	private RedisProvider redisProvider;

	/**
	* 获取值
	*/
	public String get(String key) {
		return redisProvider.get(key);
	}

	/**
	* 设置键值
	*/
	public void set(String key, String value, int timeout) {
		redisProvider.set(key, value, timeout);
	}

 ```

### 原生redis操作[不推荐]
```
#集群模式客户端
val client = (JedisCluster)RedisProvider.getClient();
#单机模式客户端
val client = (Jedis)RedisProvider.getClient();
```