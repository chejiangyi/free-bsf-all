package com.free.bsf.redis.limit;

import com.free.bsf.core.base.Callable;
import com.free.bsf.core.util.ConvertUtils;
import lombok.val;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.commands.JedisCommands;

import java.util.ArrayList;

public class CountRedisLimit extends BaseRedisLimit {
    public CountRedisLimit(Object jedis){
        super(jedis);
    }

    private static final String lua=
            "local count = tonumber(ARGV[1])   ---第一个参数 次数\n" +
            "local time = tonumber(ARGV[2])    ---第二个参数 间隔时间\n" +
            "local current = redis.call('get', KEYS[1])   ---获取key 的值\n" +
            "if current and tonumber(current) > count then    ---当这个key存在且小于规定的次数\n" +
            "    return tonumber(current)                    ---返回当前key的值\n" +
            "end\n" +
            "current = redis.call('incr', KEYS[1])   ---把key可以加1  如果不存在key就把key赋值为1\n" +
            "if tonumber(current) == 1 then     ---如果key是1\n" +
            "    redis.call('expire', KEYS[1], time)  ---设置key的过期时间\n" +
            "end\n" +
            "return tonumber(current)    ---返回当前的key的值\n";
    @Override
    public boolean visit(String key, int second, int count) {
        val keys = this.redisListParams(key);
        val args = this.redisListParams(count+"",second+"");
        val number = this.eval(lua,keys,args);
        if(ConvertUtils.convert(number,int.class)>count){
            return false;
        }
        return true;
    }
}
