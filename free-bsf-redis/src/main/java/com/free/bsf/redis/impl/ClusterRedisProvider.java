package com.free.bsf.redis.impl;

import com.free.bsf.core.base.Callable;
import com.free.bsf.core.serialize.JsonSerializer;
import com.free.bsf.core.util.ContextUtils;
import com.free.bsf.core.util.JsonUtils;
import com.free.bsf.redis.RedisException;
import com.free.bsf.redis.RedisProvider;
import com.free.bsf.redis.config.RedisProperties;
import com.free.bsf.redis.limit.RedisLimitFactory;
import com.free.bsf.redis.limit.RedisLimitType;
import lombok.Getter;
import lombok.val;
import org.springframework.util.CollectionUtils;
import redis.clients.jedis.*;
import redis.clients.jedis.params.SetParams;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.stream.Collectors;

/**
 * @author Huang Zhaoping
 */
public class ClusterRedisProvider extends AbstractRedisProvider implements RedisProvider {

    private JedisCluster jedisCluster;
    @Getter
    private RedisClusterMonitor redisMonitor;

    public ClusterRedisProvider(RedisProperties properties) {
        this.jedisCluster = initClient(properties);
        this.redisMonitor = new RedisClusterMonitor(jedisCluster);
    }

    private JedisCluster initClient(RedisProperties properties) {
        List<String> nodes = properties.getNodes();
        if (nodes == null || nodes.isEmpty()) {
            throw new RedisException("缺少bsf.redis.nodes配置");
        }
        Set<HostAndPort> addressList = new LinkedHashSet<>();
        for (String node : nodes) {
            addressList.add(HostAndPort.parseString(node));
        }
        return new JedisCluster(addressList, properties.getConnectTimeout(), properties.getSoTimeout(), properties.getMaxAttempts(), properties.getPassword(), properties.getClientName(), properties.getPool());
    }

    @Override
    public Object getClient() {
        return this.jedisCluster;
    }

    @Override
    public boolean exists(String key) {
        return this.redisMonitor.hook().run("exists",()->
            jedisCluster.exists(newKey(key))
        );
    }

    @Override
    public boolean delete(String key) {
        return this.redisMonitor.hook().run("delete",()-> {
            Long count = jedisCluster.del(newKey(key));
            return count != null && count > 0;
        });
    }

    @Override
    public boolean deleteByPrefix(String prefix) {
        Map<String, JedisPool> clusterNodes = jedisCluster.getClusterNodes();
        for (Map.Entry<String, JedisPool> stringJedisPoolEntry : clusterNodes.entrySet()) {
            Jedis resource = stringJedisPoolEntry.getValue().getResource();
            String newKey = newKey(prefix) + "*";
            Set<String> allKey = resource.keys(newKey);
            if(!CollectionUtils.isEmpty(allKey)){
                resource.del(allKey.toArray(new String[allKey.size()]));
            }
        }
        return true;
    }


    @Override
    public boolean expire(String key, int seconds) {
        return this.redisMonitor.hook().run("expire",()-> {
            return toBool(jedisCluster.expire(newKey(key), seconds));
        });
    }

    @Override
    public void set(String key, Object value) {
        this.redisMonitor.hook().run("set",()-> {
            jedisCluster.set(newKey(key), toStr(value));
            return 0;
        });
    }

    @Override
    public void set(String key, Object value, int seconds) {
        this.redisMonitor.hook().run("set",()-> {
            jedisCluster.setex(newKey(key), seconds, toStr(value));
            return 0;
        });
    }

    @Override
    public boolean setIfAbsent(String key, Object value) {
        return this.redisMonitor.hook().run("setIfAbsent",()-> {
            return toBool(jedisCluster.setnx(newKey(key), toStr(value)));
        });
    }

    @Override
    public boolean setIfAbsent(String key, Object value, int seconds) {
        return this.redisMonitor.hook().run("setIfAbsent",()-> {
            SetParams params = SetParams.setParams().nx().ex(seconds);
            return toBool(jedisCluster.set(newKey(key), toStr(value), params));
        });
    }

    @Override
    public String get(String key) {
        return this.redisMonitor.hook().run("get",()-> {
            return jedisCluster.get(newKey(key));
        });
    }

    @Override
    public <T> T get(String key, Class<T> type) {
        return this.redisMonitor.hook().run("get",()-> {
            return toObj(jedisCluster.get(newKey(key)), type);
        });
    }

    @Override
    public <T> List<T> getList(String key, Class<T> type) {
        return this.redisMonitor.hook().run("getList",()->{
            return toObj(jedisCluster.get(newKey(key)), new ParameterizedType() {
                public String getTypeName() {
                    return List.class.getTypeName();
                }

                public Type[] getActualTypeArguments() {
                    return new Type[]{type};
                }

                public Type getRawType() {
                    return List.class;
                }

                public Type getOwnerType() {
                    return null;
                }
            });
        });
    }

    @Override
    public void set(String key, String field, Object value) {
        this.redisMonitor.hook().run("set",()-> {
            jedisCluster.hset(newKey(key), field, toStr(value));
            return 0;
        });
    }

    @Override
    public <T> T get(String key, String field, Class<T> type) {
        return this.redisMonitor.hook().run("get",()-> {
            return toObj(jedisCluster.hget(newKey(key), field), type);
        });
    }

    @Override
    public void set(String key, Map<String, Object> values) {
        this.redisMonitor.hook().run("set",()-> {
            Map<String, String> strMap = new HashMap<>();
            if (values != null) {
                values.forEach((field, value) -> strMap.put(field, toStr(value)));
            }
            jedisCluster.hmset(newKey(key), strMap);
            return 0;
        });
    }

    @Override
    public <T> List<T> get(String key, String[] fields, Class<T> type) {
        return this.redisMonitor.hook().run("get",()-> {
            if (fields == null || fields.length == 0) { return Collections.emptyList();}
            List<String> list = jedisCluster.hmget(newKey(key), fields);
            if (list == null) {
            	return Collections.emptyList();
            }
            if (type == String.class) {
                return (List<T>) list;
            } else {
                return (List<T>) list.stream().filter(v -> v != null).map(s -> toObj(s, type)).collect(Collectors.toList());
            }
        });
    }

    @Override
    public <T> List<T> get(String key, Collection<String> fields, Class<T> type) {
        return this.redisMonitor.hook().run("get",()-> {
            if (fields == null || fields.size() == 0) { return Collections.emptyList();}
            return get(key, fields.toArray(new String[fields.size()]), type);
        });
    }

    @Override
    public Long increment(String key, Long step) {
        return this.redisMonitor.hook().run("increment",()->{return jedisCluster.incrBy(newKey(key), step);});
    }
    @Override
    public Object eval(String script, List<String> keys, List<String> args){
        val keys2=keys.stream().map(k->newKey(k)).toList();
        return this.redisMonitor.hook().run("eval",()->{return jedisCluster.eval(script,keys2,args);});
    }

    @Override
    public <T> T cache(String key, int timeout, Callable.Func0<T> callable, Type type) {
        return this.redisMonitor.hook().run("cache",()-> {
            String json = get(key);
            if (json != null) {
                if (NULL_VALUE.equals(json)) {
                    return null;
                }
                return JsonUtils.deserialize(json, type);
            }
            if (timeout <= 0) {
                throw new RedisException("超时时间必须大于0");
            }
            T value;
            try {
                value = callable.invoke();
            } catch (Exception e) {
                if (e instanceof RedisException) {
                    throw (RedisException) e;
                } else {
                    throw new RedisException(e);
                }
            }
            if (value != null) {
                json = new JsonSerializer().serialize(value);
            } else {
                json = NULL_VALUE;
            }
            set(key, json, timeout);
            return value;
        });
    }

    @Override
    public Lock getLock(String key) {
        return getLock(key, 0);
    }

    @Override
    public Lock getLock(String key, int timeout) {
        return this.redisMonitor.hook().run("getLock",()->{
            int tTimeOut=0;int renewTimeout=0;
            if(timeout==0){
                //不超时,自动续约
                tTimeOut=10;
                renewTimeout=5;
            }else{
                //会超时,不自动续约
                tTimeOut=timeout;
                renewTimeout=0;
            }
            return new RedisLock(this, key, tTimeOut, renewTimeout);
        });
    }

    @Override
    public boolean tryLock(String key, Runnable runnable) {
        return tryLock(key, 0, runnable);
    }

    @Override
    public boolean tryLock(String key, int timeout, Runnable runnable) {
        return this.redisMonitor.hook().run("tryLock",()->{
            Lock lock = getLock(key, timeout);
            if (lock.tryLock()) {
                try {
                    runnable.run();
                    return true;
                } finally {
                    lock.unlock();
                }
            } else {
                return false;
            }
        });
    }

    @Override
    public void lock(String key, Runnable runnable) {
        lock(key, 0, runnable);
    }

    @Override
    public void lock(String key, int timeout, Runnable runnable) {
         this.redisMonitor.hook().run("lock",()->{
            Lock lock = getLock(key, timeout);
            lock.lock();
            try {
                runnable.run();
            } finally {
                lock.unlock();
            }
            return 0;
        });
    }

    @Override
    public <T> T tryLock(String key, Callable.Func0<T> callable) {
        return tryLock(key, 0, callable);
    }

    @Override
    public <T> T tryLock(String key, int timeout, Callable.Func0<T> callable) {
        return this.redisMonitor.hook().run("tryLock",()-> {
            Lock lock = getLock(key, timeout);
            if (lock.tryLock()) {
                try {
                    return doCallable(callable);
                } finally {
                    lock.unlock();
                }
            } else {
                return null;
            }
        });
    }

    @Override
    public <T> T lock(String key, Callable.Func0<T> callable) {
        return lock(key, 0, callable);
    }

    @Override
    public <T> T lock(String key, int timeout, Callable.Func0<T> callable) {
        return this.redisMonitor.hook().run("lock",()-> {
            Lock lock = getLock(key, timeout);
            lock.lock();
            try {
                return doCallable(callable);
            } finally {
                lock.unlock();
            }
        });
    }

    private <T> T doCallable(Callable.Func0<T> callable) {
        try {
            return callable.invoke();
        } catch (Exception e) {
            if (e instanceof RuntimeException) {
                throw (RuntimeException) e;
            } else {
                throw new RedisException("Call failed in redis lock", e);
            }
        }
    }

    public boolean limit(String key, int second, int count, RedisLimitType type){
        val redisLimit = RedisLimitFactory.create(this,type);
        return redisLimit.visit(newKey("_lock_"+key),second,count);
    }

    @Override
    public void close() {
        jedisCluster.close();
    }
}
