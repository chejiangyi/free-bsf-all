package com.free.bsf.redis.limit;

import com.free.bsf.core.util.ReflectionUtils;
import lombok.val;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.commands.JedisCommands;

import java.util.ArrayList;
import java.util.List;

/**
 * 参考:
 * 1. https://blog.csdn.net/ct1104/article/details/125607714
 * 2. https://blog.csdn.net/nandao158/article/details/125330327
 */
public class BaseRedisLimit {
    protected Object jedis;
    public BaseRedisLimit(Object jedis){
        this.jedis = jedis;
    }

    public boolean visit(String key,int second,int count){
        return false;
    }

    protected <T> ArrayList<T>  redisListParams(T... args){
        val list = new ArrayList<T>(args.length);
        for(val a:args){
            list.add(a);
        }
        return list;
    }

    protected Object eval(String script, List<String> keys, List<String> args){
        return ReflectionUtils.callMethodWithParams(this.jedis,"eval",new Object[]{script,keys,args},String.class,List.class,List.class);
    }
}
