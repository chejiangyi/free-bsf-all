package com.free.bsf.redis.config;

import com.free.bsf.core.base.BsfException;
import com.free.bsf.core.config.BsfConfiguration;
import com.free.bsf.core.base.BsfEnvironmentEnum;
import com.free.bsf.core.util.LogUtils;
import com.free.bsf.core.util.PropertyUtils;
import com.free.bsf.core.util.StringUtils;
import com.free.bsf.redis.RedisException;
import com.free.bsf.redis.RedisProvider;
import com.free.bsf.redis.annotation.RedisCacheAspect;
import com.free.bsf.redis.impl.*;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.*;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;


/**
 * @author Huang Zhaoping
 **/
@Configuration
@Import(BsfConfiguration.class)
@EnableConfigurationProperties(RedisProperties.class)
@ConditionalOnProperty(name = "bsf.redis.enabled", havingValue = "true")
public class RedisConfiguration implements InitializingBean, DisposableBean {

    @Override
    public void afterPropertiesSet() {
        LogUtils.info(RedisConfiguration.class,RedisProperties.Project,"已启动,addressList:"+ StringUtils.nullToEmpty(PropertyUtils.getPropertyCache(RedisProperties.BsfRedisNodes,"")));
    }

    @Override
    public void destroy() {
        RedisLockRenew.destroy();
    }

    @Bean(destroyMethod = "close")
    public AbstractRedisProvider redisProvider(RedisProperties properties) {
        String keyPrefix = properties.getKeyPrefix();
        String springAppName = PropertyUtils.getPropertyCache(RedisProperties.SpringApplicationName,"");
        if (keyPrefix == null || keyPrefix.length() == 0) {
            // 根据spring.application.name设置默认值
            if (springAppName != null && springAppName.length() > 0) {
                keyPrefix = springAppName.replace("free-", "").replace("-provider", "").replace("-task","");
            }
        }
        AbstractRedisProvider service = null;
        if("cluster".equalsIgnoreCase(properties.getType())){
            service = new ClusterRedisProvider(properties);
        }else if("jedis".equalsIgnoreCase(properties.getType())){
            service=new JedisRedisProvider(properties);
        }else{
            throw new RedisException("配置redis type类型,指定redis是否是集群等类型！");
        }

        service.setKeyPrefix(keyPrefix);
        return service;
    }

    @Bean
    public RedisCacheAspect redisCacheAspect(RedisProvider redisProvider) {
        return new RedisCacheAspect(redisProvider);
    }

    @Lazy
    @ConditionalOnBean(RedisProvider.class)
    @Bean
    public RedisClusterMonitor redisClusterMonitor(RedisProvider redisProvider){
        if(redisProvider instanceof ClusterRedisProvider)
        { return ((ClusterRedisProvider)redisProvider).getRedisMonitor();}
        else
        {  return null;}
    }
}
