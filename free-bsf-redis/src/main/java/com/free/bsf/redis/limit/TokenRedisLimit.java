package com.free.bsf.redis.limit;

import com.free.bsf.core.util.ConvertUtils;
import lombok.val;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;

public class TokenRedisLimit extends BaseRedisLimit {
    public TokenRedisLimit(Object jedis) {
        super(jedis);
    }

    private static final String lua =
            "redis.replicate_commands();\n" +
                    "-- 参数中传递的key\n" +
                    "local key = KEYS[1]\n" +
                    "-- 令牌桶填充 最小时间间隔\n" +
                    "local update_len = tonumber(ARGV[1])\n" +
                    "-- 记录 当前key上次更新令牌桶的时间的 key\n" +
                    "local key_time = 'ratetokenprefix'..key\n" +
                    "-- 获取当前时间(这里的curr_time_arr 中第一个是 秒数，第二个是 秒数后毫秒数)，由于我是按秒计算的，这里只要curr_time_arr[1](注意：redis数组下标是从1开始的)\n" +
                    "--如果需要获得毫秒数 则为 tonumber(arr[1]*1000 + arr[2])\n" +
                    "local curr_time_arr = redis.call('TIME')\n" +
                    "-- 当前时间秒数\n" +
                    "local nowTime = tonumber(curr_time_arr[1])\n" +
                    "-- 从redis中获取当前key 对应的上次更新令牌桶的key 对应的value\n" +
                    "local curr_key_time = tonumber(redis.call('get',key_time) or 0)\n" +
                    "-- 获取当前key对应令牌桶中的令牌数\n" +
                    "local token_count = tonumber(redis.call('get',KEYS[1]) or -1)\n" +
                    "-- 当前令牌桶的容量\n" +
                    "local token_size = tonumber(ARGV[2])\n" +
                    "-- 令牌桶数量小于0 说明令牌桶没有初始化\n" +
                    "if token_count < 0 then\n" +
                    "    redis.call('set',key_time,nowTime)\n" +
                    "    redis.call('set',key,token_size -1)\n" +
                    "    return token_size -1\n" +
                    "else\n" +
                    "    if token_count > 0 then --当前令牌桶中令牌数够用\n" +
                    "        redis.call('set',key,token_count - 1)\n" +
                    "        return token_count -1   --返回剩余令牌数\n" +
                    "    else    --当前令牌桶中令牌数已清空\n" +
                    "        if curr_key_time + update_len < nowTime then --判断一下，当前时间秒数 与上次更新时间秒数  的间隔，是否大于规定时间间隔数 （update_len）\n" +
                    "            redis.call('set',key_time,nowTime)\n" +
                    "            redis.call('set',key,token_size -1)\n" +
                    "            return token_size - 1\n" +
                    "        else\n" +
                    "            return -1\n" +
                    "        end\n" +
                    "    end\n" +
                    "end\n";

    @Override
    public boolean visit(String key, int second, int count) {
        val keys = this.redisListParams(key);
        val args = this.redisListParams(second + "", count + "");
        val tokenCounts = this.eval(lua, keys, args);
        if (ConvertUtils.convert(tokenCounts, int.class) < 0) {
            return false;
        }
        return true;
    }
}
