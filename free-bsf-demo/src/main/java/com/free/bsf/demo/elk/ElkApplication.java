package com.free.bsf.demo.elk;

import com.free.bsf.core.base.BsfException;
import com.free.bsf.core.base.LimitRateException;
import com.free.bsf.core.util.PropertyUtils;
import com.free.bsf.sentinel.SentinelProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Huang Zhaoping
 */
@RestController
@SpringBootApplication
public class ElkApplication {

    @GetMapping("/test")
    public String test(){
        throw new RuntimeException("aaaa");
        //return "hello world";
    }

    public static void main(String[] args) {

        SpringApplication.run(ElkApplication.class, args);
    }

}
