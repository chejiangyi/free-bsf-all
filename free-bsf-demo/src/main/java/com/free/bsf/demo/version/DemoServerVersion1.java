package com.free.bsf.demo.version;

import java.util.Date;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * RPC 版本demo version 1
 * Robin.Wang
 * */
@RestController
@SpringBootApplication
public class DemoServerVersion1 {
    public static void main(String[] args){
        SpringApplication.run(DemoServerVersion1.class, args);
    }

    @GetMapping("/hello555")
    public Object hello555(String name) {
        return "hello version 1.0 "+ new Date();
    }
}
