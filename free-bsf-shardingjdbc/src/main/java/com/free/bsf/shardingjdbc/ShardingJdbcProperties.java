package com.free.bsf.shardingjdbc;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author: chejiangyi
 * @version: 2019-08-12 15:35
 **/
public class ShardingJdbcProperties {
    public final static String Project="Sharding-jdbc";
    public final static String SpringShardingSphereEnabled="spring.shardingsphere.enabled";
    public final static String SpringApplicationName="spring.application.name";
}
