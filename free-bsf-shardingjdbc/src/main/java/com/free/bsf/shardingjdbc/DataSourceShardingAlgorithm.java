package com.free.bsf.shardingjdbc;

import org.apache.shardingsphere.sharding.api.sharding.hint.HintShardingAlgorithm;
import org.apache.shardingsphere.sharding.api.sharding.hint.HintShardingValue;
import org.apache.shardingsphere.sharding.api.sharding.standard.StandardShardingAlgorithm;

import java.util.Collection;
import java.util.Properties;

/**
 * @author: chejiangyi
 * @version: 2019-10-24 14:17
 * 数据源分库算法
 *
 **/
public class DataSourceShardingAlgorithm implements HintShardingAlgorithm<String> {

    @Override
    public Collection<String> doSharding(Collection<String> collection, HintShardingValue<String> hintShardingValue) {
        return hintShardingValue.getValues();
    }

    @Override
    public String getType() {
        return "数据源指定";
    }

    @Override
    public void init() {

    }
    @Override
    public Properties getProps(){
        return null;
    }

    @Override
    public void setProps(Properties var1){

    }


}
