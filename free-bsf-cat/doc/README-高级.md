# Cat进阶篇幅

### 依赖配置
一般包已经通过free-bsf-starter依赖在脚手架项目中,无需额外配置
```
<dependency>
	<artifactId>free-bsf-cat</artifactId>
	<groupId>com.free.bsf</groupId>
	<version>1.7.1-SNAPSHOT</version>
</dependency>	
```

### Cat标准使用规范
默认bsf会改写cat,拦截整个调用链耗时等监控信息等,自动将bsf的health信息上传到cat中,但对业务开发无需关注,一键启用。

```
#启用cat,默认关闭
bsf.cat.enabled=false

#cat日志改写默认地址
CAT_HOME=${user.dir}/catlogs/

#cat服务器地址,必填,若继承apollo bsf配置,则无需关注
cat.server.url=

#cat的domain默认取值spring.application.name,必填,一般业务开发无需关注
spring.application.name=

#过滤拦截模式为default(单应用-内部调用),cross(微服务-调用链模式),默认为cross模式
bsf.cat.filter=cross
```

### 其他
cat 拦截mybatis sql查询,对所有sql可以进行耗时监控，理论上任何的第三方组件都可以考虑拦截监控。