## 文件服务集成进阶篇

### 配置说明
```
## bsf file  集成
##file服务的启用开关，非必须，默认true
bsf.file.enabled=true 			
##file服务默认为七牛云，可以扩展其他的第三方文件服务或者自己实现
bsf.file.provider=qiniu 	
##文件上传的重试次数，用于异常情况下的重试，如网络异常
bsf.file.retryUpload=3
##七牛云的accessKey	
bsf.file.qiniu.accessKey = 
##七牛云的securityKey	
bsf.file.qiniu.securityKey = 	
##七牛云的bucketName
bsf.file.qiniu.bucketName = 	
##七牛云的bucketUrl
bsf.file.qiniu.bucketUrl = 	 

```
关于七牛云的使用，请参阅[七牛云](https://developer.qiniu.com/)

### 使用示例
+ 快速上手版
```
	@Autowired(required = false)
	private FileProvider fileProvider;

	/**
	*  文件上传
	*/
    @PostMapping("/upload")
    public CommonResponse<String> uploadFile(MultipartFile file) throws Exception {    	
    	return CommonResponse.success(fileProvider.upload(file.getInputStream(), file.getOriginalFilename()));
    }
	/**
	* 上传服务器本地文件
	*/    
    public CommonResponse<String> uploadLocalFile(String file) throws Exception {    	
    	return CommonResponse.success(fileProvider.upload(file, FileUtils.getFileName(file)));
    }
```
+ 核心服务api
```
#文件服务支持的接口能力
public interface FileProvider {

    /**
     * 上传永久文件
     *
     * @param input 文件流
     * @param name  文件名
     * @return
     */
    String upload(InputStream input, String name);

    /**
     * 	上传永久文件
     *
     * @param filePath 上传的本地文件名
     */
    String upload(String filePath);
    /**
     * 上传临时文件(保留n个月,最终会过期自动删除)
     *
     * @param input 文件流
     * @param name  文件名
     * @return
     */
    String uploadTemp(InputStream input, String name);
    /**
     * 	上传临时文件
     *
     * @param filePath 上传的本地文件名
     */
    String uploadTemp(String filePath);

    /**
     * 上传token,用于前端上传的临时token
     * @return
     */
    UploadToken uploadToken(String name);

    /**
     * 删除文件
     *
     * @param url
     * @return
     */
    boolean delete(String url);
}
```
