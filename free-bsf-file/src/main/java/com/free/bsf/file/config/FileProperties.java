package com.free.bsf.file.config;

import com.free.bsf.core.util.PropertyUtils;
import lombok.Data;
import org.springframework.beans.PropertyAccessException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

/**
 * @author Huang Zhaoping
 */
public class FileProperties {
    public static final String PROVIDER_QINIU = "qiniu";
    public static final String PROVIDER_ALIOSSSTS = "aliossSts";

    public static String Project="File";
    public static String Prefix="bsf.file.";

    public static String RetryUpload= "bsf.file.retryUpload";

    public static String getProvider(){
        return PropertyUtils.getPropertyCache("bsf.file.provider","");
    }

    public static boolean getWarningEnabled(){
        return PropertyUtils.getPropertyCache("bsf.file.warningEnabled",false);
    }
}
