## 文件服务集成说明
### 介绍
提供统一的文件服务sdk及文件服务标准,目前集成[七牛云的文件服务](https://developer.qiniu.com/) ,阿里云文件服务(用于对外的文件cdn加速等)。
未来考虑多种实际场景支持腾讯云文件服务等。

### 文件上传的两种方式
* 采用sdk方式接入文件上传服务 [后端推荐使用]   
集成bsf框架,使用free-bsf-file组件上传。
* 通过获取临时token,直连第三方服务,如七牛云直接上传文件。[前端推荐使用]  
文件服务和sdk方式都支持获取临时token。优点:避免浪费后端服务器的上行带宽,使用用户的上行网络带宽。

### 依赖引用
一般包已经通过free-bsf-starter依赖在脚手架项目中,无需额外配置
``` 
<dependency>
	<artifactId>free-bsf-file</artifactId>
	<groupId>com.free.bsf</groupId>
	<version>1.7.1-SNAPSHOT</version>
</dependency>
```

### 配置说明
```
## bsf file  集成
##file服务的启用开关，非必须，默认false
bsf.file.enabled=false 			
```

### 使用示例
+ 快速上手
```
	@Autowired(required = false)
	private FileProvider fileProvider;

	/**
	*  文件上传
	*/
    @PostMapping("/upload")
    public CommonResponse<String> uploadFile(MultipartFile file) throws Exception {    	
    	return CommonResponse.success(fileProvider.upload(file.getInputStream(), file.getOriginalFilename()));
    }
	/**
	* 上传服务器本地文件
	*/    
    public CommonResponse<String> uploadLocalFile(String file) throws Exception {    	
    	return CommonResponse.success(fileProvider.upload(file, FileUtils.getFileName(file)));
    }
```


### 选型
[文件服务选型](doc/README-选型.md)

### 前端使用标准篇
[前端标准](doc/README-前端标准.md)

### 高级篇
* [七牛云](doc/README-七牛云.md)
* [阿里云](doc/README-阿里云.md)
