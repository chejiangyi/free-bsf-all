# core 核心包
基础核心包,用于定义业务核心协议或者常用的一些工具集。

### 入门篇
标准环境定义为dev(开发),fat(测试),uat(预发),pro(生产)环境
``` 
#spring 配置项目名
spring.application.name=free-demo-provider

#spring profile 环境变量配置,配置默认dev启动
spring.profiles.active=${env:dev}

#bsf 环境变量配置(必须)
bsf.env=${spring.profiles.active}
``` 

### 进阶篇
[进阶篇](doc/README-高级.md)
