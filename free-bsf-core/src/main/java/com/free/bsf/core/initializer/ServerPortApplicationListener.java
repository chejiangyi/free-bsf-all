package com.free.bsf.core.initializer;

import com.free.bsf.core.util.WebUtils;
import org.springframework.boot.web.context.WebServerInitializedEvent;
import org.springframework.context.ApplicationListener;

public class ServerPortApplicationListener implements ApplicationListener<WebServerInitializedEvent> {

    //获取内置tomcat端口号
    @Override
    public void onApplicationEvent(WebServerInitializedEvent event) {
        WebUtils.setServerPort(event.getWebServer().getPort());
    }
}
