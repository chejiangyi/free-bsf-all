package com.free.bsf.core.apiregistry;

public interface CoreRequestInterceptor {
    void append(RequestInfo request);
}