package com.free.bsf.core.util;

import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.ParameterMapping;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.type.TypeHandlerRegistry;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;

public class MybatisUtils {
    /**
     * 解析sql 方法名
     * */
    public static String getSqlMethodName(MappedStatement mappedStatement){
        // 得到类名，方法
        String[] strArr = mappedStatement.getId().split("\\.");
        String methodName = strArr[strArr.length - 2] + "." + strArr[strArr.length - 1];
        return methodName;
    }

    /**
     * 获取传入参数列表
     * @param configuration
     * @param boundSql
     * @return
     */
    public static List<Object> getParameterObjects(org.apache.ibatis.session.Configuration configuration, BoundSql boundSql) {
        List<Object> ps = new ArrayList<>();
        Object parameterObject = boundSql.getParameterObject();
        List<ParameterMapping> parameterMappings = boundSql.getParameterMappings();
        if (parameterMappings.size() > 0 && parameterObject != null) {
            TypeHandlerRegistry typeHandlerRegistry = configuration.getTypeHandlerRegistry();
            if (typeHandlerRegistry.hasTypeHandler(parameterObject.getClass())) {
                ps.add(parameterObject);
            } else {
                MetaObject metaObject = configuration.newMetaObject(parameterObject);
                for (ParameterMapping parameterMapping : parameterMappings) {
                    String propertyName = parameterMapping.getProperty();
                    if (metaObject.hasGetter(propertyName)) {
                        ps.add(metaObject.getValue(propertyName));
                    } else if (boundSql.hasAdditionalParameter(propertyName)) {
                        ps.add(boundSql.getAdditionalParameter(propertyName));
                    }
                }
            }
        }
        return ps;
    }
    /**
     * 解析sql语句
     *
     * @param configuration 配置
     * @param boundSql      sql
     * @return sql
     */
    public static String showSql(org.apache.ibatis.session.Configuration configuration, BoundSql boundSql) {
        Object parameterObject = boundSql.getParameterObject();
        List<ParameterMapping> parameterMappings = boundSql.getParameterMappings();
        String sql = boundSql.getSql().replaceAll("[\\s]+", " ");
        if (parameterMappings.size() > 0 && parameterObject != null) {
            TypeHandlerRegistry typeHandlerRegistry = configuration.getTypeHandlerRegistry();
            if (typeHandlerRegistry.hasTypeHandler(parameterObject.getClass())) {
                sql = sql.replaceFirst("\\?", Matcher.quoteReplacement(getParameterValue(parameterObject)));

            } else {
                MetaObject metaObject = configuration.newMetaObject(parameterObject);
                //替换？参数
                int j = 0;
                int i = 0;
                StringBuilder newSql = new StringBuilder();

                for (ParameterMapping parameterMapping : parameterMappings) {
                    String propertyName = parameterMapping.getProperty();
                    if (metaObject.hasGetter(propertyName)) {
                        j = sql.indexOf("?", i);
                        newSql.append(sql.substring(i, j));
                        i = j + 1;
                        newSql.append(Matcher.quoteReplacement(getParameterValue(metaObject.getValue(propertyName))));

                    } else if (boundSql.hasAdditionalParameter(propertyName)) {
                        j = sql.indexOf("?", i);
                        newSql.append(sql.substring(i, j));
                        i = j + 1;
                        newSql.append(Matcher.quoteReplacement(getParameterValue( boundSql.getAdditionalParameter(propertyName))));
                    }
                }
                if (i < sql.length()) {
                    newSql.append(sql.substring(i));
                }
                return newSql.toString();
            }
        }
        return sql;
    }
    /**
     * 参数解析
     *
     * @param obj
     * @return
     */
    private static String getParameterValue(Object obj) {
        String value = null;
        if (obj instanceof String) {
            value = "'" + obj.toString() + "'";
        } else if (obj instanceof Date) {
            DateFormat formatter = DateFormat.getDateTimeInstance(DateFormat.DEFAULT, DateFormat.DEFAULT, Locale.CHINA);
            value = "'" + formatter.format(obj) + "'";
        } else {
            if (obj != null) {
                value = obj.toString();
            } else {
                value = "";
            }

        }
        return value;
    }
}
