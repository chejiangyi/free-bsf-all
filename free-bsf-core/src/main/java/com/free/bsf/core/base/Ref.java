package com.free.bsf.core.base;

import lombok.Data;

/**
 * @author: chejiangyi
 * @version: 2019-08-02 11:18
 * 模拟c和.net预发中out和ref语法
 **/
@Data
public class Ref<T> {
    private volatile T data;
    public Ref(T data)
    {
        this.data = data;
    }
    public boolean isNull(){
        return data==null;
    }
}
