package com.free.bsf.core.util;

import com.free.bsf.core.base.BsfException;
import com.free.bsf.core.base.Callable;

/**
 * @Classname RetryUtils
 * @Description 重试
 * @Date 2021/4/22 21:34
 * @Created by chejiangyi
 */
public class RetryUtils {
    public static void tryCall(Integer maxRetryCount, Callable.Action0 action0, Callable.Action2<Integer,Exception> whenErrorAction1){
        Exception exp=null;
        for(Integer i=0;i<maxRetryCount;i++){
            try {
                action0.invoke();
                //success
                exp=null;
                break;
            }catch (Exception e){
                //fail
                if(exp==null)
                {exp=e;}
                //when error
                if(whenErrorAction1!=null){
                    whenErrorAction1.invoke(i,e);
                }
            }
        }
        if(exp!=null){
            throw new BsfException(exp);
        }
    }
}
