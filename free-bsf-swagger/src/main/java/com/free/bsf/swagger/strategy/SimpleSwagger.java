package com.free.bsf.swagger.strategy;

import com.free.bsf.swagger.config.SwaggerProperties;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * @Classname SimpleSwagger
 * @Description 简单 swagger api
 * @Date 2021/4/1 18:41
 * @Created by chejiangyi
 */
public class SimpleSwagger implements ISwagger {
    public Docket get() {
        return new Docket(DocumentationType.SWAGGER_2)
                //.enable(SwaggerProperties.getDefault().isEnable())
                .apiInfo(apiInfo())
                .useDefaultResponseMessages(false)
//                .securitySchemes(securitySchemes())
//                .securityContexts(securityContexts())
                .select()
                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(SwaggerProperties.getDefault().getTitle())
                .version(SwaggerProperties.getDefault().getVersion())
                .description(SwaggerProperties.getDefault().getDescription())
                .build();
    }

//    private List<ApiKey> securitySchemes() {
//        return Collections.singletonList(new ApiKey("TokenAuth", "login-token", "header"));
//    }
//
//    private List<SecurityContext> securityContexts() {
//        return Collections.singletonList(SecurityContext.builder()
//                .securityReferences(Arrays.asList(new SecurityReference("TokenAuth", new AuthorizationScope[]{new AuthorizationScope("global", "accessEverything")})))
//                .forPaths(PathSelectors.any()).build());
//    }
}
