package com.free.bsf.swagger.strategy;

import springfox.documentation.spring.web.plugins.Docket;

/**
 * @Classname BaseSwagger
 * @Description Swagger接口
 * @Date 2021/4/1 18:46
 * @Created by chejiangyi
 */
public interface ISwagger {
    Docket get();
}
