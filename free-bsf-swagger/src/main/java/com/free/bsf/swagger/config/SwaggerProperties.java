package com.free.bsf.swagger.config;

import com.free.bsf.core.config.CoreProperties;
import com.free.bsf.core.util.ContextUtils;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "bsf.swagger")
@Data
public class SwaggerProperties {
    private boolean enable=false;
    private String strategy="simple";
    private String title= CoreProperties.getApplicationName();
    private String description="";
    private String version="1.0";

    public static String Project="Swagger";
    public static String SpringMvcPathMatchMatchingStrategy="spring.mvc.pathMatch.matching-strategy";
    public static SwaggerProperties getDefault(){
        return ContextUtils.getBean(SwaggerProperties.class,false);
    }
}
