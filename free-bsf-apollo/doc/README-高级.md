# Apollo进阶篇幅

### 依赖配置
一般包已经通过free-bsf-starter依赖在脚手架项目中,无需额外配置
``` 
<dependency>
	<artifactId>free-bsf-apollo</artifactId>
	<groupId>com.free.bsf</groupId>
	<version>1.7.1-SNAPSHOT</version>
</dependency>
```

### Apollo标准使用规范
默认bsf会改写apollo的启动顺序和优先级顺序,使本地的properties配置优先apollo的配置使用;
最佳实践配置bsf底层全部改写赋值,开发人员无需关注,一键启用。
```
#bsf底层对appllo app.id默认使用spring配置的项目名,开发人员无需重复配置。
spring.application.name=free-demo-provider

#启动apoolo配置,默认关闭【必须】
bsf.apollo.enabled=false

#apollo app.id 默认使用spring.application.name,bsf底层自动赋值,开发人员无需重复配置。
app.id=${spring.application.name}

#env环境变量默认使用bsf.env变量,bsf底层自动赋值
env=${bsf.env}

#apollo服务器地址,bsf底层自动根据环境内部写死配置,若单独使用bsf,则必须手工配置。一般业务项目开发人员无需关心。
apollo.meta=

#apollo 是否启动的时候注入配置,bsf底层自动赋值,详细使用看官网。
apollo.bootstrap.enabled=true

#apollo 启动namespace,bsf底层自动赋值,详细使用看官网。默认application优先bsf配置
apollo.bootstrap.namespaces=application,bsf

#apollo热加载开关,日志加载前后 v1.2版本+ 的客户端才生效,bsf底层自动赋值,详细使用看官网。
apollo.bootstrap.eagerLoad.enabled=true

#apollo 本地配置缓存地址,bsf底层自动赋值,详细使用看官网。
apollo.cacheDir=${user.dir}/apolloConfig/

#apollo 本地properties配置优先于apollo配置,bsf底层自动赋值,改写apollo加载逻辑实现。
bsf.apollo.local.first.enabled=true
```

### 其他
1. apollo 改写cat启动类,避免与cat集成init的冲突。