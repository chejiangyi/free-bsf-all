# free-bsf-nacos
## 简介
free-bsf-nacos 是对nacos的扩展支持和bsf集成。[参考nacos srping-context使用方式](https://nacos.io/zh-cn/docs/quick-start-spring.html)

## 依赖POM

```
<dependency>
	<artifactId>free-bsf-nacos</artifactId>
	<groupId>com.free.bsf</groupId>
	<version>1.7.1-SNAPSHOT</version>
</dependency>
```
## 使用介绍
* 支持以nacos做配置中心
* 支持以nacos做注册中心

## 通用配置说明
```shell 
#应用名,必填
spring.application.name=free-bsf-demo

#可通过bsf配置开启nacos使用,默认false
bsf.nacos.enabled=false

#可通过bsf配置开启nacos配置中心使用,默认false
bsf.nacos.config.enabled=false

#可通过bsf配置开启nacos使用,默认false
bsf.nacos.discovery.enabled=false

#nacos的服务器地址,必填
nacos.server-addr=127.0.0.1:8848
```

## 以nacos做配置中心
支持nacos原生方式,也支持bsf配置方式启动
```shell 
#应用名,必填
spring.application.name=free-bsf-demo

#可通过bsf配置开启nacos使用,默认false
bsf.nacos.enabled=true

#可通过bsf配置开启nacos配置中心使用,默认false
bsf.nacos.config.enabled=true

#nacos的服务器地址,必填
nacos.server-addr=127.0.0.1:8848
```
代码举例
```
@Component
public class Config {
    @NacosValue(value = "${useLocalCache:false}", autoRefreshed = true)
    @Getter
    @Setter
    public boolean useLocalCache;
}
```


## 以nacos做注册中心
nacos注册中心配置,注:dataId默认“${应用名}”+”bsf”,应用配置优先级高于bsf配置;nacos.namespce默认“bsf当前环境变量,即dev/test/pre/pro”,故两者配置无需配置。
![nacos配置图](doc/img.png)
```
#应用名,必填
spring.application.name=free-bsf-demo

#可通过bsf配置开启nacos使用,默认false
bsf.nacos.enabled=true

#可通过bsf配置开启nacos使用,默认false
bsf.nacos.discovery.enabled=true

#nacos的服务器地址,必填
nacos.server-addr=127.0.0.1:8848
```
必须使用[bsf ApiRegistry组件包](../free-bsf-apiregistry/README.md)引用。
bsf apiRegistry配置切换为rpc使用nacos注册中心
```
#一键启用开关,默认false,重启后生效
bsf.apiRegistry.enabled = true

#支持ApiClient/FeignClient注解方式的Rpc拦截,重启后生效
bsf.apiRegistry.apiClientAspect.enabled = true

#注册中心实现，目前开启nacos注册中心
#bsf.apiRegistry.registry.type=NacosRegistry

#注册中心心跳刷新server服务信息列表时间
bsf.apiRegistry.registry.nacos.serverListCache.heartBeatTime=10000

#注册中心rpc包扫描的路径,分割多个
bsf.apiRegistry.rpcClient.basePackages = com.free.bsf.demo
```
调用方式案例(与FeignClient使用方式保持一致)
```
//兼容@FeignClient
@ApiClient(name = "lmc-test-provider",path = "/")
public interface  CustomerProvider {
    /*接口协议定义*/
    //支持GetMapping,PostMapping
    @PostMapping("/test")
    //支持@RequestParam,@RequestBody
    ApiResponseEntity<String>  test(@RequestParam("str") String str);
}
```
```
@Autowired(required = false)
CustomerProvider customerProvider;
public void test(){
    customerProvider.test("测试");
}
```

by 车江毅 
2023-02-17


