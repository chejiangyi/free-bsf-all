package com.free.bsf.nacos;

import com.alibaba.nacos.spring.context.annotation.config.NacosPropertySource;

@NacosPropertySource(dataId = "${spring.application.name}", autoRefreshed = true)
@NacosPropertySource(dataId = "bsf", autoRefreshed = true)
public class NacosDefaultPropertySourceConfiguration {
}
