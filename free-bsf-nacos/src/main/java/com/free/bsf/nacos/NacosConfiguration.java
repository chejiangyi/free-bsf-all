package com.free.bsf.nacos;

import com.alibaba.nacos.spring.context.annotation.config.NacosPropertySource;
import com.free.bsf.api.apiregistry.INacosRegistry;
import com.free.bsf.core.config.BsfConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ConditionalOnProperty(name = NacosProperties.BsfNacosEnabled, havingValue = "true")
@Import(value = { BsfConfiguration.class,NacosConfigBeanDefinitionRegistrar2.class,NacosDiscoveryBeanDefinitionRegistrar2.class,NacosDefaultPropertySourceConfiguration.class })
public class NacosConfiguration {

    @Bean(destroyMethod = "close")
    @ConditionalOnProperty(name = NacosProperties.BsfNacosDiscoveryEnabled, havingValue = "true")
    public INacosRegistry nacosRegistry(){
        return new NacosRegistry();
    }
}
