package com.free.bsf.nacos;

import com.alibaba.nacos.spring.context.annotation.discovery.EnableNacosDiscovery;
import com.alibaba.nacos.spring.context.annotation.discovery.NacosDiscoveryBeanDefinitionRegistrar;
import com.alibaba.nacos.spring.util.NacosBeanUtils;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotationMetadata;

import java.util.Map;

public class NacosDiscoveryBeanDefinitionRegistrar2 extends NacosDiscoveryBeanDefinitionRegistrar {
    private Environment environment;

    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        if(NacosProperties.discoveryEnabled()) {
            AnnotationAttributes attributes = NacosProperties.getEnableNacosDiscovery();
            NacosBeanUtils.registerGlobalNacosProperties((Map)attributes, registry, this.environment, "globalNacosProperties$discovery");
            NacosBeanUtils.registerGlobalNacosProperties((Map)attributes, registry, this.environment, "globalNacosProperties$maintain");
            NacosBeanUtils.registerNacosCommonBeans(registry);
            NacosBeanUtils.registerNacosDiscoveryBeans(registry);
        }
    }

    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }
}
