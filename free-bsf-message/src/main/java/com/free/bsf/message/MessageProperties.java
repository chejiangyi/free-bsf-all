package com.free.bsf.message;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author: chejiangyi
 * @version: 2019-08-12 15:07
 **/

public class MessageProperties {
    public static String Project="Message";
    public static String MessageEnabled="bsf.message.enabled";
    public static String MessageType="bsf.message.type";
    public static String MessageToken="bsf.message.{type}.access_token";
    public static String MessageProjectToken="bsf.message.{type}.project.access_token";
    public static String MessageCommonUrl="bsf.message.common.url";
    public static String MessageCommonContent="bsf.message.common.content";
}
