package com.free.bsf.elk;

import com.free.bsf.elk.requestid.ElkXxlJobTaskAspect;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

import com.free.bsf.elk.requestid.ElkWebTracedInterceptor;
import com.free.bsf.elk.requestid.FeginRequestInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * ELK 调用链日志配置类
 * @author Robin.Wang
 * @date	2020-04-30
 * */
@Configuration
@ConditionalOnProperty(name = "bsf.elk.request.traced.enabled", havingValue = "true")
public class ElkTracedConfiguration {

	@Bean	
	@ConditionalOnClass(name="feign.RequestInterceptor")
	public FeginRequestInterceptor feginRequestInterceptor(){
		return new FeginRequestInterceptor();
	}
	
	@Bean
	@ConditionalOnClass(name = { "org.aspectj.lang.annotation.Aspect", "com.xxl.job.core.executor.XxlJobExecutor" })
	public ElkXxlJobTaskAspect elkXxlJobTaskAspect() {
		return new ElkXxlJobTaskAspect();
	}

	@Bean
	@ConditionalOnWebApplication
	public WebMvcConfigurer elkWebTracedMvcConfigurer() {
		return new  WebMvcConfigurer() {
			@Override
			public void addInterceptors(InterceptorRegistry registry) {
				registry.addInterceptor(new ElkWebTracedInterceptor());
			}
		};
	}
}
