package com.free.bsf.elk.requestid;

import com.free.bsf.core.apiregistry.CoreRequestInterceptor;
import com.free.bsf.core.apiregistry.RequestInfo;
import lombok.val;
import org.slf4j.MDC;
import feign.RequestInterceptor;
import feign.RequestTemplate;

import java.util.HashMap;
import java.util.Map;

/**
 * Feign HTTP调用请求头增加RequestUtil.REQUEST_TRACEID参数
 * @author: chejiangyi
 * @version: 2019-08-30 13:21
 **/
public class FeginRequestInterceptor implements RequestInterceptor, CoreRequestInterceptor {

    @Override
    public void apply(RequestTemplate requestTemplate) {
        val header = newHeader();
        if(header!=null){
            for(val kv:header.entrySet()){
                requestTemplate.header(kv.getKey(),kv.getValue());
            }
        }
    }

    private Map<String,String> newHeader(){
        if(RequestUtil.getRequestId()==null) {
            RequestUtil.setRequestId(RequestUtil.getHeaderRequestId());
        }
        Map<String,String> header = new HashMap<>();
        header.put(RequestUtil.REQUEST_TRACEID, RequestUtil.getRequestId());
        return header;
    }

    @Override
    public void append(RequestInfo request) {
        val header = newHeader();
        if(header!=null){
            for(val kv:header.entrySet()){
                request.getHeader().put(kv.getKey(),kv.getValue());
            }
        }
    }
}
