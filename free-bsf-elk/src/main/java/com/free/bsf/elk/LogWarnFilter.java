package com.free.bsf.elk;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.turbo.TurboFilter;
import ch.qos.logback.core.filter.AbstractMatcherFilter;
import ch.qos.logback.core.spi.FilterReply;
import com.free.bsf.core.util.PropertyUtils;
import com.free.bsf.core.util.StringUtils;
import com.free.bsf.core.util.WarnUtils;
import lombok.val;
import org.slf4j.ILoggerFactory;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;

/**
 * @Classname LogWarnFilter
 * @Description 日志报警过滤器
 * @Date 2021/4/23 20:20
 * @Created by chejiangyi
 */
public class LogWarnFilter  extends TurboFilter {
    public LogWarnFilter init(){
        ILoggerFactory factory = LoggerFactory.getILoggerFactory();
        if (factory instanceof LoggerContext) {
            LoggerContext context = ((LoggerContext) factory);
            context.addTurboFilter(this);
        }
        return this;
    }
    @Override
    public FilterReply decide(Marker marker, Logger logger, Level level, String format,
                              Object[] params, Throwable t) {
        if(level == Level.ERROR) {
            if(PropertyUtils.getPropertyCache(ElkProperties.BsfElkWarnEnabled,false)) {
                try {
                    val message = format;
                    if (message != null && (message.startsWith("【报警】")||message.startsWith("[报警]"))) {
                        WarnUtils.notify(WarnUtils.ALARM_ERROR,
                                "日志报警",
                                StringUtils.subString3(message, 100));
                    }
                }catch (Exception e){
                    //报警失败就放弃了,避免死循环
                }
            }
        }
        return FilterReply.NEUTRAL;
    }
}
