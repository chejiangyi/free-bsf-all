package com.free.bsf.elk.requestid;

import java.util.UUID;

import com.free.bsf.core.util.WebUtils;
import lombok.val;
import org.apache.skywalking.apm.toolkit.trace.TraceContext;
import org.slf4j.MDC;

/**
 * @author: chejiangyi
 * @version: 2020-01-14 09:53
 **/
public class RequestUtil {
	/**
	 * HTTP请求trace ID,用于调用链日志追踪
	 * */
    public static final String REQUEST_TRACEID = "traceid";
    public static final String SKYWALKING_REQUEST_TRACEID = "Tid";

    public static String getRequestId(){
        return MDC.get(RequestUtil.REQUEST_TRACEID);
    }

    public static String getHeaderRequestId(){
        if(WebUtils.getRequest()!=null){
            return WebUtils.getRequest().getHeader(REQUEST_TRACEID);
        }
        return null;
    }
    /**
     * 	获取请求header中的的traceid，如果没有则生成一个UUID
     * */
    public static void setRequestId(String requestId){
        if(requestId==null) {
            MDC.put(RequestUtil.REQUEST_TRACEID, newRequestId());
        }else{
            MDC.put(RequestUtil.REQUEST_TRACEID, requestId);
        }
    }

    public static String newRequestId(){
        return UUID.randomUUID().toString().replace("-","");
    }

    public static void clearRequestId(){
        MDC.remove(RequestUtil.REQUEST_TRACEID);
    }

    public static String getSkywalkingRequestId(){
        return TraceContext.traceId();
    }

    public static void setSkywalkingRequestId(){
        if(getSkywalkingRequestId()!=null) {
            MDC.put(RequestUtil.SKYWALKING_REQUEST_TRACEID, getSkywalkingRequestId());
        }
    }

    public static void clearSkywalkingRequestId(){
        MDC.remove(RequestUtil.SKYWALKING_REQUEST_TRACEID);
    }
}
