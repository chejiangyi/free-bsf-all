package com.free.bsf.elk;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.encoder.Encoder;
import ch.qos.logback.core.filter.Filter;
import ch.qos.logback.core.spi.FilterReply;
import com.free.bsf.core.base.BsfEnvironmentEnum;
import com.free.bsf.core.base.BsfException;
import com.free.bsf.core.config.CoreProperties;
import com.free.bsf.core.util.LogUtils;
import com.free.bsf.core.util.PropertyUtils;
import com.free.bsf.core.util.StringUtils;
import com.free.bsf.core.util.WarnUtils;
import com.free.bsf.elk.requestid.RequestUtil;
import lombok.val;

import net.logstash.logback.appender.LogstashTcpSocketAppender;
import net.logstash.logback.encoder.LogstashEncoder;
import org.slf4j.ILoggerFactory;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class LogStashAppender {

    public LogStashAppender() {

    }

    public LogstashTcpSocketAppender createLogStashAppender() {
        LogstashTcpSocketAppender appender = new LogstashTcpSocketAppender();
        var destinations = ElkProperties.getDestinations();
        if (ElkProperties.getDestinations() == null || ElkProperties.getDestinations().length == 0) {
            throw new BsfException("bsf.elk.logstash.destinations未配置");
        }
        for (String destination : destinations) {
            appender.addDestination(destination);
        }

        appender.setEncoder(createEncoder());
        appender.addFilter(new RateLimiterFilter());

        ILoggerFactory factory = LoggerFactory.getILoggerFactory();
        if (factory instanceof LoggerContext) {
            LoggerContext context = ((LoggerContext) factory);
            appender.setContext(context);

            context.getLogger("ROOT").addAppender(appender);
            LogUtils.info(ElkConfiguration.class, ElkProperties.Project,
                    "已启动!!!" + " " + ElkProperties.Destinations + "=" + String.join(",", destinations));
        }
        return appender;
    }

    private Encoder<ILoggingEvent> createEncoder() {
        LogstashEncoder encoder = new LogstashEncoder();
        var appName = ElkProperties.getAppName();
        if (ElkProperties.getAppName().length() == 0) {
            appName = PropertyUtils.getPropertyCache(CoreProperties.SpringApplicationName,"");
        }
        if (appName.length() == 0) {
            throw new IllegalArgumentException("缺少appName配置");
        }
        encoder.setIncludeMdc(true);
        encoder.setIncludeMdcKeyNames(Arrays.asList(new String[]{RequestUtil.REQUEST_TRACEID}));
        encoder.setCustomFields("{\"appname\":\"" + appName + "\",\"appindex\":\"applog\",\"env\":\"" + PropertyUtils.getPropertyCache(CoreProperties.BsfEnv, "") + "\"}");
        encoder.setEncoding("UTF-8");
        return encoder;
    }

    //窗口计数限流算法
    private static class CounterRateLimiter {
        // 上一个窗口开始的时间
        private long timestamp = System.currentTimeMillis();
        // 计数器,允许误差
        private volatile Long counter = 0L;

        public CounterRateLimiter() {
        }

        public synchronized boolean tryAcquire(Integer limit) {
            long now = System.currentTimeMillis();
            // 窗口内请求数量小于阈值，更新计数放行，否则拒绝请求
            if (now - timestamp < 1000) {
                if (counter < limit) {
                    counter++;
                    return true;
                } else {
                    return false;
                }
            }
            // 时间窗口过期，重置计数器和时间戳
            counter = 0L;
            timestamp = now;
            return true;
        }
    }
    //限流过滤器
    private static class RateLimiterFilter extends Filter<ILoggingEvent> {
        CounterRateLimiter limiter = new CounterRateLimiter();
        @Override
        public FilterReply decide(ILoggingEvent event) {
            val limit= PropertyUtils.getPropertyCache(ElkProperties.BsfElkLogstashRateLimit,0);
            if(limit<=0){
                return FilterReply.NEUTRAL;
            }
            if (limiter.tryAcquire(limit)==false) {
                tryWarn(limit);
                return FilterReply.DENY;
            }
            return FilterReply.NEUTRAL;
        }
        long lastTime = 0;
        private void tryWarn(Integer limit){
            //10秒报一次
            if(new Date().getTime()-lastTime>10000) {
                lastTime = new Date().getTime();
                try {
                    WarnUtils.notify(WarnUtils.ALARM_ERROR, "logstash日志限流", "logstash检测到日志超过流量限制" + limit + "条/s,则丢弃部分日志!");
                }catch (Exception e){
                    //报警失败则放弃
                }
            }
        }
    }

}
