# Elk进阶篇幅

### 依赖引入
一般包已经通过free-bsf-starter依赖在脚手架项目中,无需额外配置
``` 
<dependency>
	<artifactId>free-bsf-elk</artifactId>
	<groupId>com.free.bsf</groupId>
	<version>1.7.1-SNAPSHOT</version>
</dependency>
```

### 配置说明

```
## bsf elk 集成
#elk服务的开关,非必须，默认false
#bsf.elk.enabled=false

## bsf elk logstash通信同步到elk
bsf.elk.logstash.enabled=false

#elk logstash的服务地址
bsf.elk.logstash.destinations=47.99.122.154:8002

#elk logstash 限流(默认为0,不限流)
bsf.elk.logstash.rateLimit=0

#elk 应用名,默认spring.application.name
bsf.elk.appName=${spring.application.name}

#启动web的http出参入参打印
bsf.elk.httpPrint.enabled=false

#启动日志统计及监控，依赖bsf.elk.web.enabled=true
bsf.health.log.statistic.enabled=false

#elk日志报警开启
bsf.elk.warn.enabled=false

#elk开启分布式链传递调用traceid
bsf.elk.request.traced.enabled=false
```

### elk 日志报警使用
开启报警支持后,错误开头输出\[报警\]或者【报警】等,会通过message组件发送。
```
1. 需要开启bsf.message.enabled=true服务,并配置好报警模板。
2. bsf.elk.warn.enabled=true 开启报警支持。
3. log.error("【报警】"+....);

#涉及的配置参考
bsf.health.enabled=true
bsf.health.warn.enabled=true
bsf.elk.enabled=true
bsf.elk.warn.enabled = true
bsf.message.enabled = true
bsf.message.type = flybook
bsf.message.flybook.project.access_token =
bsf.message.flybook.access_token = 6c26281e-20cf-46db-b4ec-a4ad8a3963e1
```
