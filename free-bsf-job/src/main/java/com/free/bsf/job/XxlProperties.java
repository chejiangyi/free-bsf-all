package com.free.bsf.job;

import com.free.bsf.core.util.PropertyUtils;
import lombok.Data;

/**
 * @author: chejiangyi
 * @version: 2019-08-12 15:02
 **/
@Data
public class XxlProperties {

    public static String getAdminAddresses(){
        return PropertyUtils.getPropertyCache("xxl.job.admin.addresses","");
    }

    public static String getAppName(){
        return PropertyUtils.getPropertyCache("xxl.job.executor.appname","");
    }

    public static String getIp(){
        return PropertyUtils.getPropertyCache("xxl.job.executor.ip","");
    }
    //匹配网段
    public static String getIpRegx(){
        return PropertyUtils.getPropertyCache("xxl.job.executor.ip.regex","");
    }
    //排除网段
    public static String getIpExgx(){
        return PropertyUtils.getPropertyCache("xxl.job.executor.ip.exgex","");
    }

    public static int getPort(){
        return PropertyUtils.getPropertyCache("xxl.job.executor.port",9999);
    }

    public static String getAccessToken(){
        return PropertyUtils.getPropertyCache("xxl.job.accessToken","");
    }

    public static String getLogPath(){
        return PropertyUtils.getPropertyCache("xxl.job.executor.logpath","job-logs");
    }

    public static int getLogRetentionDays(){
        return PropertyUtils.getPropertyCache("xxl.job.executor.logretentiondays",7);
    }

    public static String XxlJobAdminAddresses="xxl.job.admin.addresses";
}
