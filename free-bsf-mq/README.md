## MQ组件集成

### 介绍
组件集成RocketMQ和RabbitMQ,并规范MQ使用的标准用法,简化业务端的使用。
未来会考虑再集成kafka等其他的消息队列。
关于更详细RocketMQ的搭建及使用，详见[官网](http://rocketmq.apache.org/)

### MQ使用标准
业务开发常见使用RocketMQ,RabbitMQ,Kafka等mq组件;优先推荐业务开发使用RocketMQ为标准MQ选型。

### 技术选型对比
暂无

### 依赖引用
一般包已经通过free-bsf-starter依赖在脚手架项目中,无需额外配置
``` 
<dependency>
	<artifactId>free-bsf-mq</artifactId>
	<groupId>com.free.bsf</groupId>
	<version>1.7.1-SNAPSHOT</version>
</dependency>
```

### 配置说明
```
# 开启mq支持
bsf.mq.enabled = true
```

### RocketMQ 上手文档
[RocketMQ上手文档](doc/README-RocketMQ.md)

### RabbitMQ 上手文档
[RabbitMQ上手文档](doc/README-RabbitMQ.md)

