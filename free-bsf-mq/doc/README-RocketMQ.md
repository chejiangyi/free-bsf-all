## RocketMQ组件集成

### 介绍
RocketMQ 组件的集成,方便开发快速上手开发。
关于更详细RocketMQ的搭建及使用，详见[官网](http://rocketmq.apache.org/)
同时也支持阿里云RocketMQ开源版本连接(但是阿里云对自己的商业版本rocketmq改造较多,限制也很多,故只能支持简单功能)

### rocketmq 快速配置
* 入门版配置
```
# 开启mq支持
bsf.mq.enabled = true
# 开启rocketmq支持
bsf.rocketmq.enabled = true
# rocketmq服务器配置,一般开发无需关注。
bsf.rocketmq.namesrvaddr = 127.0.0.1:9876;127.0.0.1:9877
```
* 阿里云rocketmq相关配置
注意:功能优先,详情参考阿里云开源sdk使用文档。
```
bsf.mq.enabled = true
bsf.rocketmq.enabled = true
#bsf.rocketmq.aliyun.enabled = false
#bsf.rocketmq.aliyun.accesskey = 
#bsf.rocketmq.aliyun.secret = 
#bsf.rocketmq.namesrvaddr = http://MQ_INST_1307635199997505_BXhYR0By.mq-internet-access.mq-internet.aliyuncs.com:80
```

#### RocketMQ 示例代码
```
    @Autowired(required = false)
    private RocketMQProducerProvider rocketMQProducerProvider;
    @Autowired(required = false)
    private RocketMQConsumerProvider rocketMQConsumerProvider;

    public static void main(String[] args){
        ApplicationContext context = SpringApplication.run(ProducerApplication.class, args);
        
        //订阅消息
        RocketMQConsumerProvider consumerProvider = context.getBean(RocketMQConsumerProvider.class);
        consumerProvider.subscribe(new RocketMQSubscribeRunable<String>()
                .setQueueName("free-bsf-demo-test")
                .setConsumerGroup("GID_free-bsf-demo-test-consumer-01")
                .setFilterTags(null)
                .setType(String.class)
                .setRunnable((msg)->{
                    System.out.println(new JsonSerializer().serialize( msg));
                }));
        
        //发送消息
        rocketMQProducerProvider.sendMessage(new RocketMQSendMessage<String>()
                        .setTag("ddd")
                        .setQueueName("free-bsf-demo-test")
                        .setMsg("aaaa")
                );
    }
    
 ```