## RabbitMQ组件集成

### 介绍
RabbitMQ 组件的集成,方便开发快速上手开发。  
主要考虑一些历史项目的兼容使用。

### RabbitMQ 快速配置
* 入门版配置
```
# 开启mq支持
bsf.mq.enabled = true
# rabbitmq 的支持
bsf.rabbitmq.enabled = true
# rabbitmq 服务地址
bsf.rabbitmq.restUri = amqp://rabbit:XXX123@127.0.0.1:5673
```
* 进阶版本配置
```
# 当消费者线程的处理速度十分慢，而队列的消息量十分少的场景下，可以考虑把 prefetch_count设置为 3
#一般场景下，建议使用RabbitMQ官方的建议值30或者spring-boot-starter-amqp中的默认值250
bsf.rabbitmq.prefetchCount = 3
# 消费者数量
bsf.rabbitmq.consumerCount = 5
# 最大消费者
bsf.rabbitmq.maxConsumerCount = 10
bsf.rabbitmq.channelCacheSize=500
```

#### RabbitMQ 示例代码
```
    @Autowired(required = false)
    private RabbitMQProducerProvider rabbitMQProducerProvider;
    @Autowired(required = false)
    private RabbitMQConsumerProvider rabbitMQConsumerProvider;

    public static void main(String[] args){
        ApplicationContext context = SpringApplication.run(ProducerApplication.class, args);
        
        //订阅消息
        RabbitMQConsumerProvider consumerProvider = context.getBean(RabbitMQConsumerProvider.class);
        consumerProvider.subscribe(new RabbitMQSubscribeRunable()
                .setQueueName("bsf.rabbitmq.Fanout.queue.demo")
                .setType(String.class)
                .setRunnable((msg)->{
                    System.out.println("Fanout Message" + new JsonSerializer().serialize(msg));
                }));
        
        //发送消息
        val build = new RabbitMQSendMessage<String>()
               .setExchange("bsf.rabbitmq.exchange.topic.demo")
               .setExchangeTypeEnum(ExchangeTypeEnum.TOPIC)
               .setEnableDeadQueue(true)
               .setRoutingKey("bsf.rabbitmq.rk.topic.demo")
               .setMsg("bsf rabbitmq sendTopicMessage " + atomicInteger.getAndIncrement())
               .setQueueName("bsf.rabbitmq.topic.queue.demo");
        rabbitMQProducerProvider.sendMessage(build);
    }
    
 ```