package com.free.bsf.mq.rocketmq;

import com.free.bsf.core.util.PropertyUtils;
import lombok.Data;
import org.apache.rocketmq.acl.common.AclClientRPCHook;
import org.apache.rocketmq.acl.common.SessionCredentials;
import org.apache.rocketmq.remoting.RPCHook;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author: chejiangyi
 * @version: 2019-08-12 15:17
 **/
public class RocketMQProperties {
    /**
     * 阿里云是否开启支持
     */
    public static Boolean getAliyunEnabled(){
        return PropertyUtils.getPropertyCache("bsf.rocketmq.aliyun.enabled",false);
    }
    /**
     * 阿里云访问key
     */
    public static String getAliyunAccessKey(){
        return PropertyUtils.getPropertyCache("bsf.rocketmq.aliyun.accesskey","");
    }
    /**
     * 阿里云秘钥
     */
    public static String getAliyunSecret(){
        return PropertyUtils.getPropertyCache("bsf.rocketmq.aliyun.secret","");
    }
    /**
     * namesrv地址 可以有多个，;分割
     */
    public static String getNamesrvaddr(){
        return PropertyUtils.getPropertyCache("bsf.rocketmq.namesrvaddr","");
    }
    /**
     * 重试消费次数
     **/
    public static Integer getReconsumeTimes(){
        return PropertyUtils.getPropertyCache("bsf.rocketmq.reConsumeTimes",3);
    }
    /**
     * 是否启用VIP通道
     * */
    public static Boolean isUseVIPChannel(){
        return PropertyUtils.getPropertyCache("bsf.rocketmq.isUseVIPChannel",false);
    }
    /**
     * 	消费线程数量
     * */
    public static Integer getConsumeThreadMax(){
        return PropertyUtils.getPropertyCache("bsf.rocketmq.consumeThreadMax",64);
    }
    /**
     * 	消费线程数量
     * */
    public static Integer getConsumeThreadMin(){
        return PropertyUtils.getPropertyCache("bsf.rocketmq.consumeThreadMin",20);
    }
//    /**
//     *	批量消费Size
//     * */
//    @Value("${bsf.rocketmq.consumeMessageBatchMaxSize:100}")
//    private Integer consumeMessageBatchMaxSize;
    
    public static String Project="RocketMQ";
    public static String BSfRocketMQNameSrvaddr="bsf.rocketmq.namesrvaddr";

    public static RPCHook getAclRPCHook() {
        return new AclClientRPCHook(new SessionCredentials(getAliyunAccessKey(), getAliyunSecret()));
    }
}
