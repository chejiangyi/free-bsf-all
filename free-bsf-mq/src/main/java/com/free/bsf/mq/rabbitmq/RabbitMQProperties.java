package com.free.bsf.mq.rabbitmq;

import com.free.bsf.core.util.PropertyUtils;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;


/**
 * $RabbitMQProducerProvider 配置熟性信息
 *
 * @author clz.xu
 */
public class RabbitMQProperties {

    public static String Project = "RabbitMQ";
    /**
     * rabbitmq 连接地址
     */

    public static String getRestUri(){
        return PropertyUtils.getPropertyCache("bsf.rabbitmq.restUri","");
    }

    /**
     * 拉取消息数量
     */
    public static Integer getPrefetchCount(){
        return PropertyUtils.getPropertyCache("bsf.rabbitmq.prefetchCount",3);
    }

    /**
     * 并发消息者数
     */
    public static Integer getConsumerCount(){
        return PropertyUtils.getPropertyCache("bsf.rabbitmq.consumerCount",3);
    }

    /**
     * 最大并发消息者数
     */
    public static Integer getMaxConsumerCount(){
        return PropertyUtils.getPropertyCache("bsf.rabbitmq.maxConsumerCount",10);
    }

    /**
     *  缓存数量默认1024
     */
    public static Integer getChannelCacheSize(){
        return PropertyUtils.getPropertyCache("bsf.rabbitmq.channelCacheSize",500);
    }


}
